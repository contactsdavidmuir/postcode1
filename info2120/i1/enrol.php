<? require "loggedinheader.php"; ?>
<?
	if (isset($_POST['scheck'])) {
		if (isset($_POST['enrol']))
		{
			$e = $_POST['enrol'];
			if (strlen($e) > 0 )
			{
	                        # Decode our encoded course-semesteryear value
				preg_match("/^(.*?)(-)(.{2}?)(.*?)$/", $e,$matches);
				   $course = $matches[1];
				   $semester = $matches[3];
				   $year = $matches[4];
	  			require "db.php";
	  			if ($conn=oci_connect($dbUser, $dbPass, $db)) {
	     				$id = $_SESSION['id'];
	     				$stmt = oci_parse($conn, "INSERT INTO TRANSCRIPT VALUES ('$sid', '$course', '$semester', '$year' , NULL)");
	     				$stmt2 = oci_parse($conn, "UPDATE COURSEOFFERING SET ENROLLMENT = ENROLLMENT-1 WHERE CRSCODE = '$course' AND SEMESTER = '$semester' AND YEAR  = '$year' AND ENROLLMENT < MAXENROLLMENT");
	     				oci_execute($stmt, OCI_DEFAULT);
	     				oci_execute($stmt2, OCI_DEFAULT);
	
					if (!oci_commit($conn))
					{
						print "Unable to enrol, max enrollment reached.";
						print oci_error($conn);
						oci_rollback($conn);
					}
					else
					{
						print "<strong>Successfully enrolled in " .$course ." in " .$semester ." of " .$year .".</strong><br/>\n";
					}
				}
				
			}
		}
		else
		{
			print "<strong>No course selected.</strong><br/>\n";
		}

	}
?>
<h1>Enrol</h1>
<?php
  require "db.php";
  if ($conn=oci_connect($dbUser, $dbPass, $db)) {
     $id = $_SESSION['id'];
     # Don't offer the courses if they already exist in transcript.
     $stmt = oci_parse($conn, "SELECT COURSEOFFERING.CRSCODE, COURSEOFFERING.SEMESTER, COURSEOFFERING.YEAR FROM COURSEOFFERING WHERE COURSEOFFERING.CRSCODE NOT IN (SELECT CRSCODE FROM TRANSCRIPT WHERE STUDID = $id) AND ((COURSEOFFERING.SEMESTER = 'S2' AND COURSEOFFERING.YEAR = '2006') OR (COURSEOFFERING.SEMESTER = 'S1' AND COURSEOFFERING.YEAR = '2007')) ");
     oci_execute($stmt, OCI_DEFAULT);
     ?>
     <form method="post" action="<? echo $_SERVER['PHP_SELF'];?>" enctype="multipart/form-data" class="form2">
     <fieldset>
     	<legend>Enrol</legend>
     <?
     while (oci_fetch($stmt)) {# use upper-case column names in oci_result()
     	$c = oci_result($stmt, "CRSCODE");
     	$s = oci_result($stmt, "SEMESTER");
     	$y = oci_result($stmt, "YEAR");
	$a = $c ."-" .$s .$y;
        echo "<input type='radio' class ='formcheckbox' name='enrol' id='" .$a ."' value='" .$a ."'/><label for='" . $a . "'>" .$c ." (" .$s ."-" .$y .")</label><br/>\n";
     }
     ?>
     <input type="hidden" id="hidden1" name="scheck" id="scheck" value="1"/><br/>
     <input type="submit" id="submit" value="Enrol" name="submit"/>
     <input type="reset" id="reset" value="Reset" name="reset"/>

     </fieldset>
     </form>
     <?
     // Commit transaction...
     oci_commit($conn);

     // cleanup...
     oci_free_statement($stmt);
     oci_close($conn);
     
   } else{
     // logon error
     $err = oci_error();
     echo "Oracle Connect Error: " . $err['message'];
   }
?>
</table>
<? require "loggedinfooter.php"; ?>
