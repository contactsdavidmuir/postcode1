<?php session_start(); ?>
<?
	$title = "INFO2120: Assignment2";
?>
<!DOCTYPE html
	PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en-US" xml:lang="en-US">
<head>
<title><? echo $title; ?></title>
<link rel="stylesheet" type="text/css" href="enrol.css" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>
<body>
<div class="site">
<div class="menu">
        <img src="images/logo.png"/><br/>
</div>
<?php
	$url = "menu.php";
	$self = $_SERVER['PHP_SELF'];
	
	// Change the passed value of r to prevent directory traversal attacks.
    	if (isset($_GET['r'])) {
		$url_string = preg_split('/\//', $_GET['r'], -1, PREG_SPLIT_NO_EMPTY);
		$url = array_pop($url_string);
		$self = $self ."?r=" .$url;
	}
	
	if (isset($_REQUEST['scheck'])) {
		if (preg_match("/^[0-9]*$/", $_POST['sid']) && preg_match("/^[0-9A-Za-z]*$/", $_POST['password']))
		{
			require "db.php";
			if ($conn=oci_connect($dbUser, $dbPass, $db)) 
			{
			        $sid = trim($_POST['sid']);
	       	 		$password = trim($_POST['password']);
	
	        		#$stmt = oci_parse($conn, "SELECT * FROM STUDENT WHERE ID = :sid AND PASSWORD = :password");
	        		$stmt = oci_parse($conn, "SELECT * FROM STUDENT WHERE ID = '$sid' AND PASSWORD = '$password'");
				
				#Why is it only on this page that it doesn't work?
	        		#$stmt = oci_parse($conn, "SELECT * FROM STUDENT WHERE ID = :sid AND PASSWORD = :password");
				#oci_bind_by_name($stmt, ':sid', $sid);
				#oci_bind_by_name($stmt, ':password', $password);
	       			oci_execute($stmt, OCI_DEFAULT);
	
				if (OCIFetchInto($stmt, $row, OCI_ASSOC+OCI_RETURN_NULLS)) 
				{
	       		     		$_SESSION['pass'] = $row['PASSWORD'];
	       		     		$_SESSION['id'] = $row['ID'];
	       		     		$_SESSION['user'] = $row['NAME'];
			                header( "Location: " .$url );
	        		} 
				else 
				{
	                		$error = "ERROR: Incorrect SID or Password.";
	        		}
	
	        		// Commit transaction...
			        oci_commit($conn);
	
			        // cleanup...
	        		oci_free_statement($stmt);
		        	oci_close($conn);
			} 
			else 
			{
	    			$err = oci_error();
				echo "Oracle Connect Error: " . $err['message'];
			}
		}
		else
		{
			$error = "ERROR: SID can only be numeric, Password alphanumeric.";
		}
	}
?>
	<? if (isset($error)) echo "<h3>" .$error ."</h3>";  ?>
    <form method="post" class="form" action="<?php echo $self;?>">
    	<label for="sid">SID:</label><input type="text" id="sid" name="sid"/><br/>
    	<label for="password">Password:</label><input type="password" id="password" name="password"/><br/>
	<input type="hidden" id="hidden1" name="scheck" id="scheck" value="1"/>
	<input type="submit" value="Login" name="submit"/>
	<input type="reset" value="Reset" name="reset"/>
   </form>
<? require "loggedinfooter.php"; ?>
