<? require "inc/loggedinheader.php";?>
  <h1>Course Details</h1> 
  <hr/>

<?php
   # trick: check whether the form has already be submitted or not
   if ( !array_key_exists('_submit_check', $_GET) ) 
   {  # Only shows the input form if its hidden value was not sent
?>

  <form method="get" action="<?php echo $_SERVER['PHP_SELF'];?>">
    <label for="crscode">Enter Course Code: </label>
    <input type="Text" id ="crscode" name="crscode" value="">
    <input type="submit" value="submit" name="submit"><br />
    <input type="hidden" name="_submit_check" value="1"/>
  </form>

<?php
  } else {
    # retrieve any http POST data from the request header
    $CrsCode = $_GET["crscode"];

    # connect to Oracle - insert own Oracle login data!
    $db = '//oracle10g.it.usyd.edu.au/orcl';
    if ($conn=oci_connect("ikru9866", "cat", $db)) {
       
      # retrieve the data
      echo "<h2>Details of " . $CrsCode . ":</h2>\n";
      $stmt = oci_parse($conn, "SELECT * FROM Course WHERE CrsCode=:crscode");
      oci_bind_by_name($stmt, ':crscode', $CrsCode);
      oci_execute($stmt, OCI_DEFAULT);
      if ( OCIFetchInto($stmt, $row, OCI_ASSOC+OCI_RETURN_NULLS) ) {
            echo "UoS Code:  " .     $row["CRSCODE"] . "\n<br>";
            echo "UoS Title: " .     $row["CRSNAME"] . "\n<br>";
            echo "Department: " .    $row["DEPTID"]  . "\n<br>";
            echo "Credit Points: " . $row["CREDITS"] . "\n<br>";
      } else  {
         echo "No course with CrsCode '$CrsCode' found.";
      }

       // Commit transaction...
       oci_commit($conn);
       
       // cleanup
       oci_free_statement($stmt);
       oci_close($conn);
     } else {
       $err = oci_error();
       echo "Oracle Connect Error: " . $err['message'];
     }
  }
?>
<? require "inc/loggedinfooter.php"; ?>
