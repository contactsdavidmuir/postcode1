<? require "inc/loggedinheader.php"; ?>
<?
	if (isset($_POST['scheck'])) {
		if (isset($_POST['enrol']))
		{
			$e = $_POST['enrol'];
			if (strlen($e) > 0 )
			{
    	                    # Decode our encoded course-semesteryear value
				preg_match("/^(.*?)(-)(.{2}?)(.*?)$/", $e,$matches);
				   $course = $matches[1];
				   $semester = $matches[3];
				   $year = $matches[4];
  				require "inc/db.php";
	  			if ($conn=oci_connect($dbUser, $dbPass, $db)) {
	     				$id = $_SESSION['id'];
	     				$stmt = oci_parse($conn, "UPDATE TRANSCRIPT SET GRADE = 'W' WHERE STUDID = :id AND CRSCODE = :course AND SEMESTER = :semester AND YEAR  = :year");
					oci_bind_by_name($stmt, ':id', $id);
					oci_bind_by_name($stmt, ':course', $course);
					oci_bind_by_name($stmt, ':semester', $semester);
					oci_bind_by_name($stmt, ':year', $year);
					$stmt2 = oci_parse($conn, "UPDATE COURSEOFFERING SET ENROLLMENT = ENROLLMENT-1 WHERE CRSCODE = :course AND SEMESTER = :semester AND YEAR  = :year");
					oci_bind_by_name($stmt2, ':course', $course);
					oci_bind_by_name($stmt2, ':semester', $semester);
					oci_bind_by_name($stmt2, ':year', $year);
	     				oci_execute($stmt, OCI_DEFAULT);
	     				oci_execute($stmt2, OCI_DEFAULT);
	
					if (!oci_commit($conn))
					{
						print "Unable to enrol, max enrollment reached.";
						print oci_error($conn);
						oci_rollback($conn);
					}
					else
					{
					print "<strong>Successfully unenrolled from " .$course ." in " .$semester ." of " .$year .".</strong><br/>\n";
					}
				}
			}
		}
		else
		{
				print "<strong>No course selected.</strong><br/>\n";
		}
	}
?>
<h1>Withdraw</h1>
<?php
  require "inc/db.php";
  if ($conn=oci_connect($dbUser, $dbPass, $db)) {
     $id = $_SESSION['id'];
     # Don't offer the courses if they already exist in transcript.
     $stmt = oci_parse($conn, "SELECT CRSCODE, SEMESTER, YEAR FROM TRANSCRIPT WHERE STUDID = :id AND GRADE IS NULL");
     oci_bind_by_name($stmt, ':id', $id);
     oci_execute($stmt, OCI_DEFAULT);
     ?>
     <form method="post" action="<? echo $_SERVER['PHP_SELF'];?>" enctype="multipart/form-data" class="form2">
     <fieldset>
     	<legend>Withdraw</legend>
     <?
     while (oci_fetch($stmt)) {# use upper-case column names in oci_result()
     	$c = oci_result($stmt, "CRSCODE");
     	$s = oci_result($stmt, "SEMESTER");
     	$y = oci_result($stmt, "YEAR");
	$a = $c ."-" .$s .$y;
        echo "<input type='radio' class ='formcheckbox' name='enrol' id='" .$a ."' value='" .$a ."'/><label for='" . $a . "'>" .$c ." (" .$s ."-" .$y .")</label><br/>\n";
     }
     ?>
     <input type="hidden" id="hidden1" name="scheck" id="scheck" value="1"/><br/>
     <input type="submit" id="submit" value="Withdraw" name="submit"/>
     <input type="reset" id="reset" value="Reset" name="reset"/>

     </fieldset>
     </form>
     <?
     // Commit transaction...
     oci_commit($conn);

     // cleanup...
     oci_free_statement($stmt);
     oci_close($conn);
     
   } else{
     // logon error
     $err = oci_error();
     echo "Oracle Connect Error: " . $err['message'];
   }
?>
</table>
<? require "inc/loggedinfooter.php"; ?>
