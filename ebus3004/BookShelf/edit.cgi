#!/usr/bin/perl
use CGI qw/:standard/;
use CGI qw/unescapeHTML/;
use CGI::Pretty;
use DBI;
use bookshelf;
our @errors;
	if (param('id')) {
	$id = param('id');
	$dbh = DBI->connect('dbi:Pg:dbname=bookstore;host=localhost', 'bookstore', 'bookpass', { RaiseError => 1, AutoCommit => 1 }) || die "Error connecting to the database: $DBI::errstr\n";
	$query = "SELECT title, author, isbn, description, comment, rating, price, quantity from bookshelf where bookid=$id";
	@book = $dbh->selectrow_array($query);
	$title = $book[0];
	$dbh->disconnect;
	}
print 	header,
	start_html(	-title=>"Editing: $title",
			-style=>'/~bookstore/BookShelf/bookshelf.css'); 
	require 'menu.cgi';
print	"<div class='site'>";
print	h1('Edit');
	if (!url_param('id')) {
		print "you shouldnt be here.";
	}
	elsif (param('delete') && !param('confirmdelete')) {
	print	div( {-class=> "form"}, start_form(-method=>'post'),
		fieldset(
		legend('Confirm Delete'),
		p(
			'Are you sure you want to delete this book:', br(),
			strong('Title:'), param('title'), br(),
			strong('Author:'), param('author'), br(),
			strong('ISBN:'), param('isbn'), br(),
			strong('Price:'), param('price'), br(),
			strong('Quantity:'), param('quantity'), br(),
			strong('Description:'), param('description'), br(),
			strong('Comment:'), param('comment'), br(),
			strong('Rating:'), param('rating'), br(),
			hidden(		-name=>'confirmdelete', 
					-value=>"confirmdelete",
					-id=>"hidden1"),
			hidden(	-name=>'id', 
				-value=>"$id",
				-id=>"hidden1"),
			hidden(		-name=>'title', 
					-value=>"$title",
					-id=>"hidden1"),
			),
		submit('submit', 'Delete'),
		end_form,));
	}
	elsif (param('confirmdelete')) {
		deleteBook($id);
		print p("Book \"$title\" successfully deleted.");
		
	}
	elsif (!param('title')) {
		printForm(@book);
	}
	else {
		@book = (param('title'), param('author'), param('isbn'), param('description'), param('comment'), param('rating'), param('price'), param('quantity'));
		$validForm = parseForm(@book);
		if ($validForm) {
			updateBook(escapeHTML($id), escapeHTML($title), escapeHTML($author), escapeHTML($isbn), escapeHTML($description), escapeHTML($comment), escapeHTML($rating), escapeHTML($price), escapeHTML($quantity));
			print p("Successfully updated details for: $title");
		}
		else {
			printForm();
		}
	}
print	"</div>";
print	end_html;

sub printForm {
	($title, $author, $isbn, $description, $comment, $rating, $price, $quantity) = @_;
	$utitle = unescapeHTML($title);
	$uauthor = unescapeHTML($author);
	$udescription = unescapeHTML($description);
	$ucomment = unescapeHTML($comment);
	if ($#errors > -1) {
	print '<ul>';
	foreach $error (@errors) {
		 print li($error);
	}
	print '</ul>';
	}

	print	div( {-class=> "form"}, start_form(-method=>'post'),
		fieldset(
		legend('Details'),
		hidden(	-name=>'id', 
			-value=>"$id",
			-id=>"hidden1"),
		"<label for='title'>Title:</label>", textfield(	-maxlength=>50,
								-name=>'title',
								-id=>'title',
								-default=>"$utitle"),
		"<label for='author'>Author:</label>", textfield(	-maxlength=>50,
									-name=>'author',
									-id=>'author',
									-default=>"$uauthor"),
		"<label for='isbn'>ISBN:</label>", textfield(	-maxlength=>10, 
								-name=>'isbn',
								-id=>'isbn',
								-default=>"$isbn"),
		),
		fieldset(
		legend('Cart'),
		"<label for='price'>Price:</label>", textfield(	-maxlength=>13, 
								-name=>'price',
								-id=>'price',
								-default=>"$price"),
		"<label for='quantity'>Quantity:</label>", textfield(	-maxlength=>10, 
								-name=>'quantity',
								-id=>'quantity',
								-default=>"$quantity"),
		),

		fieldset( 
		legend('Review'),
		"<label for='description'>Description:</label>", textarea(	-rows=> '10',
						-cols=>'20',
						-name=>'description',
						-id=>'description',
						-default=>"$udescription",
),
		"<label for='comment'>Comment:</label>", textarea(	-rows=> '10',
									-cols=> '20',
									-name =>'comment',
									-id=>'comment',
									-default=>"$ucomment",
),p,
		"<label for='rating'>Rating:</label>", popup_menu(	-name=>'rating',
									-id=>'rating',
					-value=>['1','2','3','4','5'],
					-default=>"$rating",),
		),
		fieldset(
		legend('Remove Book'),
		"<label for='deletebutton'>Delete:</label>",
		checkbox(	-name=>'delete',
				-id=>'deletebutton',
				-label=>'',
),
		),
		submit('submit', 'Update Book'),
		reset('reset', 'Reset'),
		end_form,);
}

sub parseForm {
	($title, $author, $isbn, $description, $comment, $rating, $price, $quantity) = @_;
	$validForm = 1;
	if (length($title) < 1) {
		$validForm = 0;
		$error = "You forgot to add a title.";
		push @errors, $error;
	}
	if (length($author) < 1) {
		$validForm = 0;
		$error = "You forgot to add an author.";
		push @errors, $error;
	}
	if(!( $isbn =~ m/^(\d{10})|(\d{9}X)$/)) {
		$error = "ISBN is not valid. 10 Digits or 9 Digits with an X, no punctuation.";
		push @errors, $error;
	}
	if(!( $price =~ m/^\d{1,10}(\.\d{2})?$/)) {
		$validForm = 0;
		$error = "Price is invalid. X.XX or X";
		push @errors, $error;
	}
	if(!( $quantity =~ m/^\d{1,}$/)) {
			$validForm = 0;
			$error = "Quantity is invalid.";
			push @errors, $error;
	}
	if (length($description) > 1000) {
		$validForm = 0;
		$error = "Length of description (" .length($description) . ") exceeds maximum of 1000 characters.";
		push @errors, $error;
	}
	if (length($comment) > 1000) {
		$validForm = 0;
		$error = "Length of comment (" .length($comment) . ") exceeds maximum of 1000 characters.";
		push @errors, $error;
	}
	return $validForm;
}


sub updateBook {
	($id, $title, $author, $isbn, $description, $comment, $rating, $price, $quantity) = @_;
	$dbh = DBI->connect('dbi:Pg:dbname=bookstore;host=localhost', 'bookstore', 'bookpass', { RaiseError => 1, AutoCommit => 1 }) || die "Error connecting to the database: $DBI::errstr\n";
	$query = "UPDATE bookshelf SET title = '$title', author = '$author', isbn = '$isbn', description = '$description', comment = '$comment', rating = $rating, price = '$price', quantity = $quantity where bookid = $id";
	$sth = $dbh->prepare($query);
	$sth->execute;
	$dbh->disconnect;

}

sub deleteBook {
	$id = $_[0];
	if($id =~ m/\d+/) {
		$dbh = DBI->connect('dbi:Pg:dbname=bookstore;host=localhost', 'bookstore', 'bookpass', { RaiseError => 1, AutoCommit => 1 }) || die "Error connecting to the database: $DBI::errstr\n";
		$query = "DELETE from bookshelf where bookid = $id";
		$sth = $dbh->prepare($query);
		$sth->execute;
		$dbh->disconnect;
	}
	else {
		print p('Possible hacking attempt logged.');
	}
}
