--
-- PostgreSQL database dump
--

SET client_encoding = 'UTF8';
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: cart; Type: TABLE; Schema: public; Owner: ivan; Tablespace: 
--

CREATE TABLE cart (
    cartid character varying(32) NOT NULL,
    ip inet,
    bookid integer NOT NULL,
    quantity integer,
    creditcardno numeric(16,0),
    cvv numeric(3,0),
    purchasedate date DEFAULT ('now'::text)::date
);


ALTER TABLE public.cart OWNER TO ivan;

--
-- Data for Name: cart; Type: TABLE DATA; Schema: public; Owner: ivan
--

COPY cart (cartid, ip, bookid, quantity, creditcardno, cvv, purchasedate) FROM stdin;
b61cbb22eb9f160da21724fe3a9abbc5	127.0.0.1	105	3	1923012390213091	323	2006-10-03
b97a876d271803336882f159f7b89057	127.0.0.1	107	1	1828282828282828	232	2006-10-03
9b8d2040ebd86177af001492d23d47bc	127.0.0.1	104	3	1231231231231231	123	2006-10-04
b9aafab236f81b2f3ebb81fe1d2105d8	127.0.0.1	103	5	8888888888888888	666	2006-10-04
b9aafab236f81b2f3ebb81fe1d2105d8	127.0.0.1	107	18	8888888888888888	666	2006-10-04
b9aafab236f81b2f3ebb81fe1d2105d8	127.0.0.1	110	9	8888888888888888	666	2006-10-04
\.


--
-- Name: cart_bookid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: ivan
--

ALTER TABLE ONLY cart
    ADD CONSTRAINT cart_bookid_fkey FOREIGN KEY (bookid) REFERENCES bookshelf(bookid);


--
-- Name: cart; Type: ACL; Schema: public; Owner: ivan
--

REVOKE ALL ON TABLE cart FROM PUBLIC;
REVOKE ALL ON TABLE cart FROM ivan;
GRANT ALL ON TABLE cart TO ivan;
GRANT INSERT,SELECT,UPDATE ON TABLE cart TO bookstore;


--
-- PostgreSQL database dump complete
--

