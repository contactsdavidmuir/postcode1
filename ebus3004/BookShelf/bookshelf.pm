#!/usr/bin/perl
package bookshelf;
use Exporter;
use CGI;
our @ISA = qw(Exporter);
our @EXPORT = qw(parseFormBook);

sub parseFormBook {
	($title, $author, $isbn, $description, $comment, $rating, $price, $quantity, $submitText) = @_;
	if (length($title) < 1) {
		$error = "You forgot to add a title.";
		push @errors, $error;
	}
	if (length($author) < 1) {
		$error = "You forgot to add an author.";
		push @errors, $error;
	}
	if(!( $isbn =~ m/^(\d{10})|(\d{9}X)$/)) {
		$error = "ISBN is not valid. 10 Digits or 9 Digits with an X, no punctuation.";
		push @errors, $error;
	}
	if(!( $price =~ m/^\d{1,10}(\.\d{2})?$/)) {
		$error = "Price is invalid. X.XX or X";
		push @errors, $error;
	}
	if(!( $quantity =~ m/^\d{1,}$/)) {
			$error = "Quantity is invalid.";
			push @errors, $error;
	}
	if (length($description) > 1000) {
		$error = "Length of description (" .length($description) . ") exceeds maximum of 1000 characters.";
		push @errors, $error;
	}
	if (length($comment) > 1000) {
		$error = "Length of comment (" .length($comment) . ") exceeds maximum of 1000 characters.";
		push @errors, $error;
	}
	return @errors;
}

return 1;
