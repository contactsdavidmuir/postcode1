#!/usr/bin/perl
use CGI qw/:standard/;
use CGI::Pretty;
use CGI::Session;
use DBI;
$session = new CGI::Session();
print $session->header(),
	start_html(	-title=>"Cart contents",
			-style=>'/~ivan/BookShelf/bookshelf.css'); 
	require 'menu.cgi';
print	"<div class='site'>";
	$dbh = DBI->connect('dbi:Pg:dbname=bookdb;host=localhost', 'bookstore', 'bookpass', { RaiseError => 1, AutoCommit => 1 }) || die "Error connecting to the database: $DBI::errstr\n";
if	(param('checkout')) {
		$creditcardno = param('creditcardno');
		$cvv = param('cvv');
	        if(!( $creditcardno =~ m/^\d{16}$/)) {
               	 	$error = "Credit card number is invalid. 16 digits, no punctuation.";
               	 	push @errors, $error;
        	}
	        if(!( $cvv =~ m/^\d{3}$/)) {
               	 	$error = "CVV number is invalid. 3 digits, no punctuation.";
               	 	push @errors, $error;
        	}
		if ($#errors > -1) {
			showCartContents(@errors);
		}
		else
		{
			checkout($creditcardno, $cvv);
		}
	}
else {
print	h1('Cart contents');
	showCartContents();
	}
print	"</div>";
print	end_html;

sub showCartContents {
		@errors = @_;
		if ($#errors > -1) {
		print '<ul>';
		foreach $error (@errors) {
			 print li($error);
		}
		print '</ul>';
		}
		print start_form(-class=>"form");
		$totalQuantity = 0;
		$totalPrice = 0;
		print 	"<table>",
			caption('Cart'),
			Tr({-align=>center,-valign=>top},
		[
			th(['Title', 'Quantity', 'Price']),
		]
 	);
	foreach $param (sort $session->param) {
		$bookId = $param;
		$bookId =~ s/q//;
		$query = "SELECT title, price FROM bookshelf where bookid = $bookId";
		$sth = $dbh->prepare($query);
		$sth->execute;
		while (@book = $sth->fetchrow) {
			$quant = $session->param($param);
			if (length($quant) >= 1 && $quant != 0) {
				$price = $quant*$book[1];
				print "<tr align='center' valign='top'>", td("$book[0]"), td($quant), td('$' .$price), "</tr>";
				$totalQuantity += $quant;
				$totalPrice += $price;
			}
		}
	}
				print "<tr align='center' valign='top'>", td(strong("Subtotal")), td(strong($totalQuantity)), td(strong('$' .$totalPrice)), "</tr>";
		print 	"</table>",br;
		print 	fieldset(
			legend('Payment Details'),
			"<label for='creditcardno'>Credit Card #:</label>", textfield(	-name=>'creditcardno',
								-id=>'creditcardno',
								-maxlength=>16),
			"<label for='cvv'>CVV:</label>", textfield(	-name=>'cvv',
									-id=>'cvv',
									-maxlength=>3),
			),
			submit(-name=>'checkout', -value=>'Checkout', -id=>'checkout'),
			end_form;


}

sub checkout {
	($creditcardno , $cvv) = @_;
	$sufficientStock = 1;
	$cartid = $session->id;
	@stockShortage;
	$ip = $ENV{REMOTE_ADDR};
	foreach $param (sort $session->param) {
 		$quantity =  $session->param($param);
		$bookid = $param;
		$bookid =~ s/q//;
		if ($quantity > 0) {
			$query = "SELECT quantity, title FROM bookshelf where bookid = $bookid";
			$sth = $dbh->prepare($query);
			$sth->execute;
			while (@book = $sth->fetchrow) {
 				if ($quantity > $book[0]) {
					$sufficientStock = 0;
					$shortage = "Your choice of $quantity copies of \"$book[1]\" exceeds our stock levels ($book[0]). <br/>";
					push @stockShortage, $shortage;
				}
			}
		}
	}
	if (!$sufficientStock) {
		print p(@stockShortage, br, 'This could be as a result of someone completing their order. Please update your quantities by clicking on <a href="shop">shop</a>');
	}
	else {
		$purchasesMade = 0;
		foreach $param (sort $session->param) {
			$purchasesMade = 1;
 			$quantity =  $session->param($param);
			$bookid = $param;
			$bookid =~ s/q//;
			if ($quantity > 0) {
				$query = "UPDATE bookshelf set quantity = quantity - $quantity where bookid = $bookid";
				$sth = $dbh->prepare($query);
				$sth->execute;
				$query = "INSERT into cart values ('$cartid', '$ip' , '$bookid', '$quantity', '$creditcardno', '$cvv')";
				$sth = $dbh->prepare($query);
				$sth->execute;
			}
		}
		if ($purchasesMade) {
			print p("Successfully finished checkout process.");
		}
		else {
			print p("Cart is empty. Did you forget to add products to your cart?");
		}
		$session->delete();
	}
}
