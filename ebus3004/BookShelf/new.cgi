#!/usr/bin/perl
use CGI qw/:standard/;
use CGI::Pretty;
use DBI;
use bookshelf;
$submitText = 'Add Book';
print 	header,
	start_html(	-title=>"Add a book",
			-style=>'bookshelf.css'); 
	require 'menu.cgi';
print	"<div class='site'>";
print	h1('Add a book');
	if (!param) {
		printFormBook();
	}
	else {
		@book = (param('title'), param('author'), param('isbn'), param('description'), param('comment'), param('rating'), param('price'), param('quantity'));
		@errors = parseFormBook(@book);
		if ($#errors < 0) {
			($title, $author, $isbn, $description, $comment, $rating, $price, $quantity) = @book;
			addBook(escapeHTML($title), escapeHTML($author), escapeHTML($isbn), escapeHTML($description), escapeHTML($comment), escapeHTML($rating));
			print p(
			'Successfully added the following book:', br(),
			strong('Title:'), param('title'), br(),
			strong('Author:'), param('author'), br(),
			strong('ISBN:'), param('isbn'), br(),
			strong('Price:'), param('price'), br(),
			strong('Quantity:'), param('quantity'), br(),
			strong('Description:'), param('description'), br(),
			strong('Comment:'), param('comment'), br(),
			strong('Rating:'), param('rating'), br(),
			);
		}
		else {
			printFormBook(@errors);
		}
	}
print	"</div>";
print	end_html;


sub printFormBook {
	@errors = @_;
	if ($#errors > -1) {
	print '<ul>';
	foreach $error (@errors) {
		 print li($error);
	}
	print '</ul>';
	}
	print	start_form(-class=>"form"),
		fieldset(
		legend('Details'),
		"<label for='title'>Title:</label>", textfield(	-name=>'title',
								-id=>'title',
								-maxlength=>50,
								-default=>"$title"),
		"<label for='author'>Author:</label>", textfield(	-name=>'author',
									-id=>'author',
									-maxlength=>50,
									-default=>"$author"),
		"<label for='isbn'>ISBN:</label>", textfield(	-maxlength=>10, 
								-name=>'isbn',
								-id=>'isbn',
								-default=>"$isbn"),
		),
		fieldset(
		legend('Cart'),
		"<label for='price'>Price:</label>", textfield(	-maxlength=>13, 
								-name=>'price',
								-id=>'price',
								-default=>"$price"),
		"<label for='quantity'>Quantity:</label>", textfield(	-maxlength=>10, 
								-name=>'quantity',
								-id=>'quantity',
								-default=>"$quantity"),
		),
		fieldset( 
		legend('Review'),
		"<label for='description'>Description:</label>", textarea(	-rows=> '10',
						-cols=>'20',
						-name=>'description',
						-id=>'description',
						-default=>"$description"),
		"<label for='comment'>Comment:</label>", textarea(	-rows=> '10',
									-cols=> '20',
									-name =>'comment',
									-id=>'comment',
									-default=>"$comment"),
		"<label for='rating'>Rating:</label>", popup_menu(	-name=>'rating',
									-id=>'rating',
									-value=>['1','2','3','4','5'],
									-default=>"$rating"),
		),
		submit('submit', "$submitText"),
		reset('reset', 'Reset'),
		end_form;
}


sub addBook {
	($title, $author, $isbn, $description, $comment, $rating) = @_;
	$dbh = DBI->connect('dbi:Pg:dbname=bookdb;host=localhost', 'bookstore', 'bookpass', { RaiseError => 1, AutoCommit => 1 }) || die "Error connecting to the database: $DBI::errstr\n";
	$query = "INSERT into bookshelf values (nextval('bookshelf_bookid_seq'::regclass), '$title', '$author', '$isbn', '$description', '$comment', $rating, $price, $quantity)";
	$sth = $dbh->prepare($query);
	$sth->execute;
	$dbh->disconnect;
}
