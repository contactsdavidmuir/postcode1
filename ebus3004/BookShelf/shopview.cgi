#!/usr/bin/perl
use CGI qw/:standard/;
use CGI::Pretty;
use CGI::Session;
use DBI;
our %quantity;
$session = new CGI::Session();
print 	$session->header,
	start_html(	-title=>"Shop for books",
			-style=>'/~ivan/BookShelf/bookshelf.css'); 
	require 'menu.cgi';
print	"<div class='site'>";
print	h1('Shop for books');
	showBooks();
	if (param) {
			print 	"<table>",
			caption('Cart additions'),
			Tr({-align=>center,-valign=>top},
		[
			th(['Title', 'Quantity']),
		]
		);
		foreach $param (param) {
			$enteredQuantity = param($param);
			# If user entered a quantity greater than we
			# have in stock, we set their entry to the max.
			if (param($param) > $quantity{$param}) {
				#param(-name=>"$param", -value=>"$quantity{$param}");
				$enteredQuantity = $quantity{$param};
			}
			if (!($enteredQuantity =~ m/^\d*$/)) {
				$enteredQuantity = '';
			}
			$session->param("$param", "$enteredQuantity");
			if ($enteredQuantity > 0) {
print "<tr align='center' valign='top'>", td("$title{$param}"), td($enteredQuantity), "</tr>";

			}
		}
		print "</table>";
	}
print	"</div>";
print	end_html;

sub showBooks() {
	$dbh = DBI->connect('dbi:Pg:dbname=bookdb;host=localhost', 'bookstore', 'bookpass', { RaiseError => 1, AutoCommit => 1 }) || die "Error connecting to the database: $DBI::errstr\n";
	$query = "SELECT * FROM bookshelf";
	$sth = $dbh->prepare($query);
	$sth->execute;
	print start_form(-class=>'shopform');
	print 	'<table>',
		'<caption>Books</caption>',
		Tr({-align=>center,-valign=>top},
		[
			th(['Title','Author','ISBN', 'Description', 'Comment', 'Rating', 'Price', 'Quantity', 'Purchase']),
		]
 	);
	while (@book = $sth->fetchrow)
	{
		$q = $session->param("q$book[0]");
		$quantity{"q$book[0]"} = $book[8];
		$title{"q$book[0]"} = $book[1];
		print "<tr align='center' valign='top' >".td(["$book[1]", "$book[2]", "$book[3]", "$book[4]", "$book[5]", "$book[6]", "\$$book[7]", "<label for='q$book[0]'>$book[8]</label> ", "" .
			textfield(-name=>"q$book[0]", -maxlength=>length($book[8]), -id=> "q$book[0]", -class=>'shop', -value=>"$q")
			])."</tr>";
	}
	$dbh->disconnect();
	print '</table>';
	print br,submit(-id=>'shopButtonSubmit', -value=>'Add to Cart'), reset(-id=>'shopButtonReset'), end_form;
	print p("Alter the quantity for the books you wish to purchase. If you enter a quantity higher than what we have in stock we'll alter your entry to match our stock levels.");
}
