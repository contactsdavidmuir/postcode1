#!/usr/bin/perl
use CGI qw/:standard/;
use CGI::Pretty;
use unijobs;

print	header,
	start_html(	-title=>"Unijobs.com.au: Reset Password",
			-style=>"unijobs.css");
print	"<div class='site'>",
	pheader(),
	h1("Reset Password");
	if (!param) {
		resetPasswordForm();
	}
	else {
		resetPasswordFormErrors();
	}
print	pfooter(),
	"</div>",
	end_html;
