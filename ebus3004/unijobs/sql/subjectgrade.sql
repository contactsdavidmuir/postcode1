--
-- PostgreSQL database dump
--

SET client_encoding = 'UTF8';
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

ALTER TABLE ONLY public.subjectgrade DROP CONSTRAINT subjectgrade_studentid_fkey;
ALTER TABLE ONLY public.subjectgrade DROP CONSTRAINT subjectgrade_pkey;
DROP TABLE public.subjectgrade;
SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: subjectgrade; Type: TABLE; Schema: public; Owner: ivan; Tablespace: 
--

CREATE TABLE subjectgrade (
    gradeid serial NOT NULL,
    studentid serial NOT NULL,
    subject character varying(40) NOT NULL,
    grade numeric NOT NULL,
    "year" numeric(4,0) NOT NULL,
    semester numeric(1,0),
    CONSTRAINT graderange CHECK (((grade >= (0)::numeric) AND (grade <= (100)::numeric)))
);


--
-- Name: subjectgrade_gradeid_seq; Type: SEQUENCE SET; Schema: public; Owner: ivan
--

SELECT pg_catalog.setval(pg_catalog.pg_get_serial_sequence('subjectgrade', 'gradeid'), 1, true);


--
-- Name: subjectgrade_studentid_seq; Type: SEQUENCE SET; Schema: public; Owner: ivan
--

SELECT pg_catalog.setval(pg_catalog.pg_get_serial_sequence('subjectgrade', 'studentid'), 1, false);


--
-- Data for Name: subjectgrade; Type: TABLE DATA; Schema: public; Owner: ivan
--

COPY subjectgrade (gradeid, studentid, subject, grade, "year", semester) FROM stdin;
1	1	ebus3004 designing busses	89	2006	1
\.


--
-- Name: subjectgrade_pkey; Type: CONSTRAINT; Schema: public; Owner: ivan; Tablespace: 
--

ALTER TABLE ONLY subjectgrade
    ADD CONSTRAINT subjectgrade_pkey PRIMARY KEY (gradeid);


--
-- Name: subjectgrade_studentid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: ivan
--

ALTER TABLE ONLY subjectgrade
    ADD CONSTRAINT subjectgrade_studentid_fkey FOREIGN KEY (studentid) REFERENCES student(studentid) ON UPDATE CASCADE;


--
-- PostgreSQL database dump complete
--

