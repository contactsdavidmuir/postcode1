--
-- PostgreSQL database dump
--

SET client_encoding = 'UTF8';
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

ALTER TABLE ONLY public.job DROP CONSTRAINT job_employerid_fkey;
ALTER TABLE ONLY public.job DROP CONSTRAINT job_pkey;
DROP TABLE public.job;
SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: job; Type: TABLE; Schema: public; Owner: ivan; Tablespace: 
--

CREATE TABLE job (
    jobid serial NOT NULL,
    jobtitle character varying(60) NOT NULL,
    description character varying(1024) NOT NULL,
    datelisted date DEFAULT ('now'::text)::date,
    status character varying(60),
    frompaybracket integer,
    topaybracket integer,
    startdate date,
    enddate date,
    employerid character varying(30),
    streetaddress character varying(60),
    postcode numeric(4,0),
    suburb character varying(25),
    state character varying(3)
);


--
-- Name: job_jobid_seq; Type: SEQUENCE SET; Schema: public; Owner: ivan
--

SELECT pg_catalog.setval(pg_catalog.pg_get_serial_sequence('job', 'jobid'), 2, true);


--
-- Data for Name: job; Type: TABLE DATA; Schema: public; Owner: ivan
--

COPY job (jobid, jobtitle, description, datelisted, status, frompaybracket, topaybracket, startdate, enddate, employerid, streetaddress, postcode, suburb, state) FROM stdin;
1	President	President of our company.	2006-10-25	Hiring	6000	30000	2006-10-25	2006-12-25	1	1 Company street	2000	somewhere	NSW
2	Vice President	Executive Vice President of our company.	2006-10-25	Hiring	12	100	2006-10-28	2007-12-25	1	1 Company street	2000	somewhere	NSW
\.


--
-- Name: job_pkey; Type: CONSTRAINT; Schema: public; Owner: ivan; Tablespace: 
--

ALTER TABLE ONLY job
    ADD CONSTRAINT job_pkey PRIMARY KEY (jobid);


--
-- Name: job_employerid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: ivan
--

ALTER TABLE ONLY job
    ADD CONSTRAINT job_employerid_fkey FOREIGN KEY (employerid) REFERENCES employer(employerid) ON UPDATE CASCADE;


--
-- PostgreSQL database dump complete
--

