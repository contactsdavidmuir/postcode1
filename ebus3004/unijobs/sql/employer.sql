--
-- PostgreSQL database dump
--

SET client_encoding = 'UTF8';
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

ALTER TABLE ONLY public.employer DROP CONSTRAINT employer_pkey;
DROP TABLE public.employer;
SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: employer; Type: TABLE; Schema: public; Owner: ivan; Tablespace: 
--

CREATE TABLE employer (
    employerid serial NOT NULL,
    "password" character varying(32) NOT NULL,
    company character varying(30) NOT NULL,
    contactname character varying(40) NOT NULL,
    phonenumber character varying(15) NOT NULL,
    email character varying(60),
    companyposition character varying(40)
);


--
-- Name: employer_employerid_seq; Type: SEQUENCE SET; Schema: public; Owner: ivan
--

SELECT pg_catalog.setval(pg_catalog.pg_get_serial_sequence('employer', 'employerid'), 3, true);


--
-- Data for Name: employer; Type: TABLE DATA; Schema: public; Owner: ivan
--

COPY employer (employerid, "password", company, contactname, phonenumber, email, companyposition) FROM stdin;
1	a	McDonalds	Ronald McDonald	041111111111111	ronald@mcdonald.com	CEO
2	b	Microsoft	Bill	8	bill@g.com	VP
3	b	Microsoft	Bill	8	bill@g.com	VP
\.


--
-- Name: employer_pkey; Type: CONSTRAINT; Schema: public; Owner: ivan; Tablespace: 
--

ALTER TABLE ONLY employer
    ADD CONSTRAINT employer_pkey PRIMARY KEY (employerid);


--
-- PostgreSQL database dump complete
--

