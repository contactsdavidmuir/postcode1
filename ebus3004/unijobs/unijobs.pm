#!/usr/bin/perl
package unijobs;
use Exporter;
use CGI qw/:standard/;
use CGI::Session;
use DBI;
use Digest::MD5 qw(md5 md5_hex);
our @ISA = qw(Exporter);
our @EXPORT = qw(pheader pfooter userForm userFormErrors employerForm employerFormErrors jobForm jobFormErrors gradeForm gradeFormErrors resetPasswordForm resetPasswordFormErrors searchStudentForm searchStudentFormErrors searchJobForm searchJobFormErrors showStudents showJobs loginForm loginFormErrors showTranscript);

$session = new CGI::Session();

$dbName = 'ivan';
$dbHost = 'localhost';
# 5432 for direct conn, 5433 for pgpool
$dbPort = '5432';
$dbUser = 'ivan';
$dbPass = 'PASSWORD';

$sendmail = "/usr/sbin/sendmail -t";
$from = "From: ivan.kk\@gmail.com\n";
$to = "To: ivan\@localhost\n";

sub pheader {
	div( {-class=> "menu"}, 
		img({src=>'img/logo.png', alt=>'UniJobs.com.au logo'}),br, 
		a({href=>"login.cgi"}, "Login"), 
		a({href=>"newstudent.cgi"}, "New Student"), 
		a({href=>"updatestudent.cgi"}, "Update Student"), 
		a({href=>"newemployer.cgi"}, "New Employer"), 
		a({href=>"updateemployer.cgi"}, "Update Employer"), 
		a({href=>"newjob.cgi"}, "New Job"), 
		a({href=>"updatejob.cgi"}, "Update Job"), 
		a({href=>"newgrade.cgi"}, "New Grade"), 
		a({href=>"updategrade.cgi"}, "Update Grade"), 
		a({href=>"searchstudent.cgi"}, "Search For Students"),
		a({href=>"searchjob.cgi"}, "Search For Jobs"),
	);
}

sub pfooter {
	div( {-class=>"footer"},
		hr(),
		"&#169; 2006 DIJ Design",br,
		"Valid xhtml 1.0. Valid css. Meets section 508 and WAI standards. Optimised for firefox.",
	);
}



# perhaps put user getting code call here. eg. if
# $ENV{REQUEST_URI} == newuser, ignore, else if
# == update user @user = getUserDetails($sid);
# Allows for code reuse in new/add user pages.
# Similar for employee.
# Do we need to use CGI::Session as a result?
if($ENV{REQUEST_URI} =~ m/\/newstudent.cgi/) {
		$submitText = "Create Account";
}

elsif($ENV{REQUEST_URI} =~ m/\/newemployer.cgi/) {
		$submitText = "Create Account";
}
elsif($ENV{REQUEST_URI} =~ m/\/newjob.cgi/) {
		$submitText = "Post Job";
}
elsif($ENV{REQUEST_URI} =~ m/\/newgrade.cgi/) {
		$submitText = "Add Subject";
}
elsif($ENV{REQUEST_URI} =~ m/\/login.cgi/) {
		$submitText = "Login";
}
elsif($ENV{REQUEST_URI} =~ m/\/searchstudent.cgi/) {
		$submitText = "Search For Suitable Student";
}
elsif($ENV{REQUEST_URI} =~ m/\/searchjob.cgi/) {
		$submitText = "Search For Job";
}
elsif($ENV{REQUEST_URI} =~ m/[^\?]\/updatestudent.cgi$/) {
	$id = 1;
	($id, $firstname, $lastname, $email, $dob, $phonenumber, $password, $streetaddress, $postcode, $state, $suburb, $gpa, $degree, $major, $yearcommenced, $completionyear, $keywords, $afdate, $atdate) = getUserDetails($id);
	if ($dob =~ /(....)-(..)-(..)/) { $year= $1; $month = $2; $day = $3; } 
	if ($afdate =~ /(....)-(..)-(..)/) { $afyear= $1; $afmonth = $2; $afday = $3; } 
	if ($atdate =~ /(....)-(..)-(..)/) { $atyear= $1; $atmonth = $2; $atday = $3; } 
	$submitText = "Update Student";
}
elsif($ENV{REQUEST_URI} =~ m/[^\?]\/updatejob.cgi$/) {
	$id = 2;
	($id, $jobtitle, $description, $datelisted, $status, $frompaybracket, $topaybracket, $startdate, $enddate, $employerid, $streetaddress, $postcode, $suburb, $state) = getJobDetails($id);
	if ($startdate =~ /(....)-(..)-(..)/) { $startyear= $1; $startmonth = $2; $startday = $3; } 
	if ($enddate =~ /(....)-(..)-(..)/) { $endyear= $1; $endmonth = $2; $endday = $3; } 
	$submitText = "Update Job";
}

elsif($ENV{REQUEST_URI} =~ m/[^\?]\/updateemployer.cgi$/) {
	$id = 1;
	($id, $password, $company, $contactname, $phonenumber, $email, $companyposition) = getEmployerDetails($id);
	$submitText = "Update Employer";
}
elsif($ENV{REQUEST_URI} =~ m/[^\?]\/updategrade.cgi$/) {
	$id = 1;
	($id, $studentid, $subject, $grade, $year, $semester) = getGradeDetails($id);
	$semester = "Semester $semester";
	$submitText = "Update Subject";
}


##### Begin student form code
sub userForm {
	@states = ('NSW', 'VIC', 'QLD', 'SA', 'WA', 'NT', 'ACT', 'TAS');
	@months = (1 .. 12);
	#Todo:
	#@months = qw( Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec );
	@days = (1 .. 31);
	@years = reverse((1970 .. 1990));
	@yearscommenced = (2000 .. 2006);
	@afyears = (2006 .. 2010);
	@atyears = (2006 .. 2016);
	@completionyears = (2006 .. 2016);
	($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
	if (!$afday && !$atday) { $afday = $atday = $mday;}
	if (!$afmonth && !$atmonth) { $afmonth = $atmonth = $mon+1;}
	if (!$afyear && !$atyear) { $afyear = $atyear = $year+1900;}

	print
	div( {-class=> "form"},
 		start_form,
		fieldset(
		legend('Personal Details'),
		"<label for='firstname'>First Name:</label>", textfield(	-name=>'firstname',
								-id=>'firstname',
								-maxlength=>30,
								-default=>"$firstname"),
		"<label for='lastname'>Last Name:</label>", textfield(	-name=>'lastname',
								-id=>'lastname',
								-maxlength=>30,
								-default=>"$lastname"),
		"<label for='email'>Email:</label>", textfield(	-name=>'email',
								-id=>'email',
								-maxlength=>60,
								-default=>"$email"),
		"<label for='password'>Password:</label>", password_field(	-name=>'password',
								-id=>'password',
								-maxlength=>60,
								-default=>""),
		"<label for='confirmpassword'>Confirm Password:</label>", password_field(	-name=>'confirmpassword',
								-id=>'confirmpassword',
								-maxlength=>60,
								-default=>""),
		fieldset(
		legend('DOB'),
			"<label for='day'>Day:</label>", popup_menu(	-name=>'day',
										-id=>'day',
										-value=>[@days],
										-default=>"$day"),
			"<label for='month'>Month:</label>", popup_menu(	-name=>'month',
										-id=>'month',
										-value=>[@months],
										-default=>"$month"),
			"<label for='year'>Year:</label>", popup_menu(	-name=>'year',
										-id=>'year',
										-value=>[@years],
										-default=>"$year"),
		),
		"<label for='phonenumber'>Phone Number:</label>", textfield(	-name=>'phonenumber',
								-id=>'phonenumber',
								-maxlength=>15,
								-default=>"$phonenumber"),
		"<label for='streetaddress'>Street Address:</label>", textfield(	-name=>'streetaddress',
								-id=>'streetaddress',
								-maxlength=>60,
								-default=>"$streetaddress"),
		"<label for='postcode'>Post Code:</label>", textfield(	-name=>'postcode',
								-id=>'postcode',
								-maxlength=>4,
								-default=>"$postcode"),
		"<label for='suburb'>Suburb:</label>", textfield(	-name=>'suburb',
								-id=>'suburb',
								-maxlength=>20,
								-default=>"$suburb"),
		"<label for='state'>State:</label>", popup_menu(	-name=>'state',
									-id=>'state',
									-value=>[@states],
									-default=>"$state"),
		),
		fieldset(
		legend('Degree'),
		"<label for='gpa'>GPA:</label>", textfield(	-name=>'gpa',
								-id=>'gpa',
								-maxlength=>50,
								-default=>"$gpa"),
		"<label for='degree'>Degree:</label>", textfield(	-name=>'degree',
								-id=>'degree',
								-maxlength=>60,
								-default=>"$degree"),
		"<label for='major'>Major:</label>", textfield(	-name=>'major',
								-id=>'major',
								# Change to 60 for double/triple major
								-maxlength=>30,
								-default=>"$major"),
		"<label for='yearcommenced'>Year Commenced:</label>", popup_menu(	-name=>'yearcommenced',
									-id=>'yearcommenced',
									-value=>[@yearscommenced],
									-default=>"$yearcommenced"),
		"<label for='completionyear'>Completion Year:</label>", popup_menu(	-name=>'completionyear',
									-id=>'completionyear',
									-value=>[@completionyears],
									-default=>"$completionyear"),
		),
		fieldset( 
		legend('Other'),
		"<label for='keywords'>Keywords:</label>", textarea(	-rows=> '10',
						-cols=>'20',
						-name=>'keywords',
						-id=>'keywords',
						-default=>"$keywords"),
		),
		fieldset(
		legend('Available From'),
			"<label for='afday'>Day:</label>", popup_menu(	-name=>'afday',
										-id=>'afday',
										-value=>[@days],
										-default=>"$afday"),
			"<label for='afmonth'>Month:</label>", popup_menu(	-name=>'afmonth',
										-id=>'afmonth',
										-value=>[@months],
										-default=>"$afmonth"),
			"<label for='afyear'>Year:</label>", popup_menu(	-name=>'afyear',
										-id=>'afyear',
										-value=>[@afyears],
										-default=>"$afyear"),
		),
		fieldset(
		legend('Available Till'),
			"<label for='atday'>Day:</label>", popup_menu(	-name=>'atday',
										-id=>'atday',
										-value=>[@days],
										-default=>"$atday"),
			"<label for='atmonth'>Month:</label>", popup_menu(	-name=>'atmonth',
										-id=>'atmonth',
										-value=>[@months],
										-default=>"$atmonth"),
			"<label for='atyear'>Year:</label>", popup_menu(	-name=>'atyear',
										-id=>'atyear',
										-value=>[@atyears],
										-default=>"$atyear"),
		),
		fieldset(
		submit('submit', "$submitText"),
		reset('reset', 'Reset'),
		),
		end_form,
	);
}

sub parseUserForm {
	@student = @_;
	($firstname, $lastname, $email, $password, $confirmpassword, $day, $month, $year, $phonenumber, $streetaddress, $postcode, $suburb, $state, $gpa, $degree, $major, $yearcommenced, $completionyear, $keywords, $afday, $afmonth, $afyear, $atday, $atmonth, $atyear) = @student;
	push @errors, checkMissingValueForm($firstname,"First Name");
	push @errors, checkAlphaNumericForm($firstname,"First Name");
	push @errors, checkMissingValueForm($lastname,"Last Name");
	push @errors, checkAlphaNumericForm($lastname,"Last Name");
	push @errors, checkMissingValueForm($email,"Email");
	push @errors, checkValidEmailForm($email,"Email");
	if (!( $submitText =~ m/^Update/)) {
		push @errors, checkMissingValueForm($password,"Password");
		push @errors, checkMissingValueForm($confirmpassword,"Confirm Password");
	}
	push @errors, checkAlphaNumericForm($password,"Password");
	push @errors, checkAlphaNumericForm($confirmpassword,"Confirm Password");
	if ($password ne $confirmpassword) {
		$error = "Passwords don't match.";
		push @errors, $error;
	}
	push @errors, checkMissingValueForm($phonenumber,"Phone Number");
	push @errors, checkNumericForm($phonenumber,"Phone Number");
	push @errors, checkMissingValueForm($streetaddress,"Street Address");
	push @errors, checkAlphaNumericForm($streetaddress,"Street Address");
	push @errors, checkMissingValueForm($postcode,"Post Code");
	push @errors, checkValidPostcodeForm($postcode,"Post Code");
	push @errors, checkMissingValueForm($suburb,"Suburb");
	push @errors, checkAlphaNumericForm($suburb,"Suburb");
	push @errors, checkMissingValueForm($state,"State");
	push @errors, checkAlphaNumericForm($state,"State");
	push @errors, checkMissingValueForm($gpa,"GPA");
	push @errors, checkNumericForm($gpa,"GPA");
	push @errors, checkMissingValueForm($degree,"Degree");
	push @errors, checkAlphaNumericForm($degree,"Degree");
	push @errors, checkMissingValueForm($major,"Major");
	push @errors, checkAlphaNumericForm($major,"Major");
	push @errors, checkMissingValueForm($yearcommenced,"Year Commenced");
	push @errors, checkNumericForm($yearcommenced,"Year Commenced");
	push @errors, checkMissingValueForm($completionyear,"Completion Year");
	push @errors, checkNumericForm($completionyear,"Completion Year");
	if ($gpa > 100 || $gpa < 0) {
		$error = "Invalid <em>GPA</em> entered. Must be between 0 and 100.";
		push @errors, $error;
	}
	#more form validation goes here.
	return @errors;
}

sub getUserDetails {
	($studentid) = @_;
	#input validation on student, should never be false.
	if(!( $studentid =~ m/^\d{1,}$/)) {
		return NULL;
	}
	$dbh = DBI->connect("dbi:Pg:dbname='$dbName';host='$dbHost';port='$dbPort';port='$dbPort'", "$dbUser", "$dbPass", { RaiseError => 1, AutoCommit => 1 }) || die "Error connecting to the database: $DBI::errstr\n";
        $query = "SELECT * FROM student where studentid = ?";
        $sth = $dbh->prepare($query);
	$sth->bind_param(1, "$studentid");
        $sth->execute;
	@student = $sth->fetchrow;
	return @student;
}


sub userFormErrors
{
	@errors = parseUserForm(param('firstname'), param('lastname'), param('email'), param('password'), param('confirmpassword'), param('day'), param('month'), param('year'), param('phonenumber'), param('streetaddress'), param('postcode'), param('suburb'), param('state'), param('gpa'), param('degree'), param('major'), param('yearcommenced'), param('completionyear'), param('keywords'), param('afday'), param('afmonth'), param('afyear'), param('atday'), param('atmonth'), param('atyear'));
	if ($#errors > -1) {
		print "<ul>\n";
		$validated = 1;
		foreach $error (@errors) {
			 if (length($error)) {
				$validated = 0;
                         	print li($error);
			}
                }
		print "</ul>\n";
	}
	if (!$validated) {
		userForm();
	}
	else
	{
		if ($submitText eq "Create Account") { 
			addUser(); 
		}
		elsif ($submitText eq "Update Student") { 
			updateUser($id); 
		}
	}
}

sub addUser {
	$dbh = DBI->connect("dbi:Pg:dbname='$dbName';host='$dbHost';port='$dbPort';port='$dbPort'", "$dbUser", "$dbPass", { RaiseError => 1, AutoCommit => 1 }) || die "Error connecting to the database: $DBI::errstr\n";
	$query = "INSERT into student values (nextval('student_studentid_seq'::regclass), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        $sth = $dbh->prepare($query);
	$sth->bind_param(1, "$firstname");
	$sth->bind_param(2, "$lastname");
	$sth->bind_param(3, "$email");
	$sth->bind_param(4, "$year-$month-$day");
	$sth->bind_param(5, "$phonenumber");
	$sth->bind_param(6, md5_hex($password));
	$sth->bind_param(7, "$streetaddress");
	$sth->bind_param(8, "$postcode");
	$sth->bind_param(9, "$state");
	$sth->bind_param(10, "$suburb");
	$sth->bind_param(11, "$gpa");
	$sth->bind_param(12, "$degree");
	$sth->bind_param(13, "$major");
	$sth->bind_param(14, "$yearcommenced");
	$sth->bind_param(15, "$completionyear");
	$sth->bind_param(16, "$keywords");
	$sth->bind_param(17, "$afyear-$afmonth-$afday");
	$sth->bind_param(18, "$atyear-$atmonth-$atday");
        $sth->execute;
	print "Congratulations <strong>$firstname</strong> Your account has successfully been created, you may proceed to login with it.";
	$sth = $dbh->prepare("select last_value from student_studentid_seq");
    $sth->execute();
    my $studentid;
	while (($studentid) = $sth->fetchrow()) {
		print  "<br><br>Your User ID is: <strong><big>$studentid<big></strong>";
	}
}

sub updateUser {
	($id) = @_;
	$dbh = DBI->connect("dbi:Pg:dbname='$dbName';host='$dbHost';port='$dbPort';port='$dbPort'", "$dbUser", "$dbPass", { RaiseError => 1, AutoCommit => 1 }) || die "Error connecting to the database: $DBI::errstr\n";
	if (length($password) > 0) {
		$query = "UPDATE student set firstname=?, lastname=?, email=?, dob=?, phonenumber=?, password=?, streetaddress=?, postcode=?, state=?, suburb=?, gpa=?, degree=?, major=?, yearcommenced=?, completionyear=?, keywords=?, availablefrom=?, availabletill=? where studentid=?";
	        $sth = $dbh->prepare($query);
		$sth->bind_param(1, "$firstname");
		$sth->bind_param(2, "$lastname");
		$sth->bind_param(3, "$email");
		$sth->bind_param(4, "$year-$month-$day");
		$sth->bind_param(5, "$phonenumber");
		$sth->bind_param(6, md5_hex($password));
		$sth->bind_param(7, "$streetaddress");
		$sth->bind_param(8, "$postcode");
		$sth->bind_param(9, "$state");
		$sth->bind_param(10, "$suburb");
		$sth->bind_param(11, "$gpa");
		$sth->bind_param(12, "$degree");
		$sth->bind_param(13, "$major");
		$sth->bind_param(14, "$yearcommenced");
		$sth->bind_param(15, "$completionyear");
		$sth->bind_param(16, "$keywords");
		$sth->bind_param(17, "$afyear-$afmonth-$afday");
		$sth->bind_param(18, "$atyear-$atmonth-$atday");
		$sth->bind_param(19, "$id");
	}
	else {
		$query = "UPDATE student set firstname=?, lastname=?, email=?, dob=?, phonenumber=?, streetaddress=?, postcode=?, state=?, suburb=?, gpa=?, degree=?, major=?, yearcommenced=?, completionyear=?, keywords=?, availablefrom=?, availabletill=? where studentid=?";
	        $sth = $dbh->prepare($query);
		$sth->bind_param(1, "$firstname");
		$sth->bind_param(2, "$lastname");
		$sth->bind_param(3, "$email");
		$sth->bind_param(4, "$year-$month-$day");
		$sth->bind_param(5, "$phonenumber");
		$sth->bind_param(6, "$streetaddress");
		$sth->bind_param(7, "$postcode");
		$sth->bind_param(8, "$state");
		$sth->bind_param(9, "$suburb");
		$sth->bind_param(10, "$gpa");
		$sth->bind_param(11, "$degree");
		$sth->bind_param(12, "$major");
		$sth->bind_param(13, "$yearcommenced");
		$sth->bind_param(14, "$completionyear");
		$sth->bind_param(15, "$keywords");
		$sth->bind_param(16, "$afyear-$afmonth-$afday");
		$sth->bind_param(17, "$atyear-$atmonth-$atday");
		$sth->bind_param(18, "$id");
	}
	$sth->execute;
	print "<strong>$firstname</strong> Your account has successfully been updated.";
}

##### End student form code
#
##### Begin Input Validation Code

sub checkMissingValueForm {
	($value, $name) = @_;
	if (length($value) < 1) {
		$error = "Missing field <em>$name</em>.";
		return $error;
	}
}

sub checkValidPostcodeForm {
	($value, $name) = @_;
	if (length($value) > 0 && ($value > 8000 || $value < 1000)) {
		$error = "Invalid value in <em>$name</em>.";
	}
}

sub checkNumericForm {
	($value, $name) = @_;
	if(!( $value =~ m/^[0-9 ]*$/)) {
		$error = "Invalid format for <em>$name</em>. Must be numeric. You entered: " .escapeHTML($value);
		return $error;
	}
}

sub checkAlphaNumericForm {
	($value, $name) = @_;
	if(!( $value =~ m/^[0-9A-Za-z ]*$/)) {
		$error = "Invalid format for <em>$name</em>. Must be alphanumeric. You entered: " .escapeHTML($value);
		return $error;
	}
}

sub checkValidEmailForm {
	($value, $name) = @_;
	#alphanum (opt: + .) @ alphanum. domain
	if(!( $value =~ m/^(([0-9A-Za-z_-]+\+?\.?)+\@([0-9A-Za-z_-]+\.)+[A-Za-z]{2,4})?$/)) {
		$error = "Invalid format for <em>$name</em>. Must be a valid email address. You entered: " .escapeHTML($value);
		return $error;
	}
}

sub checkMatchingPasswordsForm {
	($password, $password2) = @_;
	if($password != $password2) {
		$error = "Passwords don't match.";
		return $error;
	}
}
##### End Input Validation code
#
##### Begin employer form code
sub employerForm {
	print
	div( {-class=> "form"},
 		start_form,
		fieldset(
		legend('Company Details'),
		"<label for='company'>Company:</label>", textfield(	-name=>'company',
								-id=>'company',
								-maxlength=>30,
								-default=>"$company"),
		"<label for='contactname'>Contact Name:</label>", textfield(	-name=>'contactname',
								-id=>'contactname',
								-maxlength=>30,
								-default=>"$contactname"),
		"<label for='companyposition'>Company Position:</label>", textfield(	-name=>'companyposition',
								-id=>'companyposition',
								-maxlength=>60,
								-default=>"$companyposition"),
		),
		fieldset(
		legend('Contact Details'),
		"<label for='email'>Email:</label>", textfield(	-name=>'email',
								-id=>'email',
								-maxlength=>60,
								-default=>"$email"),
		"<label for='phonenumber'>Phone Number:</label>", textfield(	-name=>'phonenumber',
								-id=>'phonenumber',
								-maxlength=>15,
								-default=>"$phonenumber"),
		),
		fieldset(
		legend('Password'),
		"<label for='password'>Password:</label>", password_field(	-name=>'password',
								-id=>'password',
								-maxlength=>60,
								-default=>""),
		"<label for='confirmpassword'>Confirm Password:</label>", password_field(	-name=>'confirmpassword',
								-id=>'confirmpassword',
								-maxlength=>60,
								-default=>""),
		),
		fieldset(
		submit('submit', "$submitText"),
		reset('reset', 'Reset'),
		),
		end_form,
	);
}


sub parseEmployerForm {
	@employer = @_;
	($password, $confirmpassword, $company, $contactname, $phonenumber, $email, $companyposition) = @employer;
	push @errors, checkMissingValueForm($company,"Company");
	push @errors, checkAlphaNumericForm($company, "Company");
	push @errors, checkMissingValueForm($contactname,"Contact Name");
	push @errors, checkAlphaNumericForm($contactname,"Contact Name");
	push @errors, checkMissingValueForm($companyposition,"Company Position");
	push @errors, checkAlphaNumericForm($companyposition,"Company Position");
	push @errors, checkMissingValueForm($email,"Email");
	push @errors, checkValidEmailForm($email,"Email");
	push @errors, checkMissingValueForm($phonenumber,"Phone Number");
	push @errors, checkNumericForm($phonenumber,"Phone Number");
	if (!( $submitText =~ m/^Update/)) {
		push @errors, checkMissingValueForm($password,"Password");
		push @errors, checkMissingValueForm($confirmpassword,"Confirm Password");
	}
	push @errors, checkAlphaNumericForm($password,"Password");
	push @errors, checkAlphaNumericForm($confirmpassword,"Confirm Password");
	if ($password ne $confirmpassword) {
		$error = "Passwords don't match.";
		push @errors, $error;
	}
	return @errors;
}

sub employerFormErrors
{
	@errors = parseEmployerForm(param('password'), param('confirmpassword'), param('company'), param('contactname'), param('phonenumber'), param('email'), param('companyposition'));
	if ($#errors > -1) {
		print "<ul>\n";
		$validated = 1;
		foreach $error (@errors) {
			 if (length($error)) {
				$validated = 0;
                         	print li($error);
			}
                }
		print "</ul>\n";
	}
	if (!$validated) {
		employerForm();
	}
	else
	{
		if ($submitText eq "Create Account") { 
			addEmployer(); 
		}
		elsif ($submitText eq "Update Employer") { 
			updateEmployer($id); 
		}
	}
}

sub addEmployer {
	$dbh = DBI->connect("dbi:Pg:dbname='$dbName';host='$dbHost';port='$dbPort';port='$dbPort'", "$dbUser", "$dbPass", { RaiseError => 1, AutoCommit => 1 }) || die "Error connecting to the database: $DBI::errstr\n";
	$query = "INSERT into employer values (nextval('employer_employerid_seq'::regclass), ?, ?, ?, ?, ?, ?)";
        $sth = $dbh->prepare($query);
	$sth->bind_param(1, md5_hex($password));
	$sth->bind_param(2, "$company");
	$sth->bind_param(3, "$contactname");
	$sth->bind_param(4, "$phonenumber");
	$sth->bind_param(5, "$email");
	$sth->bind_param(6, "$companyposition");
        $sth->execute;
	print "Congratulations <strong>$contactname</strong> Your account has successfully been created, you may proceed to login with it.";
	$sth = $dbh->prepare("select last_value from employer_employerid_seq");
    $sth->execute();
    my $employerid;
	while (($employerid) = $sth->fetchrow()) {
		print  "<br><br>Your User ID is: <strong><big>$employerid<big></strong>";
	}
}

sub updateEmployer {
	($id) = @_;
	$dbh = DBI->connect("dbi:Pg:dbname='$dbName';host='$dbHost';port='$dbPort';port='$dbPort'", "$dbUser", "$dbPass", { RaiseError => 1, AutoCommit => 1 }) || die "Error connecting to the database: $DBI::errstr\n";
	if (length($password) > 0) {
		$query = "UPDATE employer set password=?, company=?, contactname=?, phonenumber=?, email=?, companyposition=? where employerid=?";
       		$sth = $dbh->prepare($query);
		$sth->bind_param(1, md5_hex($password));
		$sth->bind_param(2, "$company");
		$sth->bind_param(3, "$contactname");
		$sth->bind_param(4, "$phonenumber");
		$sth->bind_param(5, "$email");
		$sth->bind_param(6, "$companyposition");
		$sth->bind_param(7, "$id");
	}
	else {
		$query = "UPDATE employer set company=?, contactname=?, phonenumber=?, email=?, companyposition=? where employerid=?";
       		$sth = $dbh->prepare($query);
		$sth->bind_param(1, "$company");
		$sth->bind_param(2, "$contactname");
		$sth->bind_param(3, "$phonenumber");
		$sth->bind_param(4, "$email");
		$sth->bind_param(5, "$companyposition");
		$sth->bind_param(6, "$id");
	}
        $sth->execute;
	print "<strong>$contactname</strong> Your account has successfully been updated.";
}

sub getEmployerDetails {
	($employerid) = @_;
	#input validation on student, should never be false.
	if(!( $employerid =~ m/^\d{1,}$/)) {
		return NULL;
	}
	$dbh = DBI->connect("dbi:Pg:dbname='$dbName';host='$dbHost';port='$dbPort';port='$dbPort'", "$dbUser", "$dbPass", { RaiseError => 1, AutoCommit => 1 }) || die "Error connecting to the database: $DBI::errstr\n";
        $query = "SELECT * FROM employer where employerid = ?";
        $sth = $dbh->prepare($query);
	$sth->bind_param(1, "$employerid");
        $sth->execute;
	@employer = $sth->fetchrow;
	return @employer;
}
##### End employer form code
#
#### Begin job form code
sub jobForm {
	@months = (1 .. 12);
	#Todo:
	#@months = qw( Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec );
	@states = qw(NSW VIC QLD SA WA NT ACT TAS);
	@days = (1 .. 31);
	@yearscommenced = (2000 .. 2006);
	@statuses = qw(Hiring Interviewing Filled);
	@startyears = (2006 .. 2010);
	@endyears = (2006 .. 2016);
	($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
	if (!$startday && !$endday) { $startday = $endday = $mday;}
	if (!$startmonth && !$endmonth) { $startmonth = $endmonth = $mon+1;}
	if (!$startyear && !$endyear) { $startyear = $endyear = $year+1900;}
	print
	div( {-class=> "form"},
 		start_form,
		fieldset(
		legend('Job Details'),
		"<label for='jobtitle'>Job Title:</label>", textfield(	-name=>'jobtitle',
								-id=>'jobtitle',
								-maxlength=>60,
								-default=>"$jobtitle"),
		"<label for='frompaybracket'>Lowest Salary:</label>", textfield(	-name=>'frompaybracket',
								-id=>'frompaybracket',
								-maxlength=>30,
								-default=>"$frompaybracket"),
		"<label for='topaybracket'>Highest Salary:</label>", textfield(	-name=>'topaybracket',
								-id=>'topaybracket',
								-maxlength=>60,
								-default=>"$topaybracket"),
			"<label for='status'>Job Status:</label>", popup_menu(	-name=>'status',
										-id=>'status',
										-value=>[@statuses],
										-default=>"$status"),
		),
		fieldset(
		legend('Job Description'),
		"<label for='description'>Job Description:</label>", textarea(	-rows=> '10',
						-cols=>'20',
						-name=>'description',
						-id=>'keywords',
						-default=>"$description"),
		),
		fieldset(
		legend('Start Date'),
			"<label for='startday'>Day:</label>", popup_menu(	-name=>'startday',
										-id=>'startday',
										-value=>[@days],
										-default=>"$startday"),
			"<label for='startmonth'>Month:</label>", popup_menu(	-name=>'startmonth',
										-id=>'startmonth',
										-value=>[@months],
										-default=>"$startmonth"),
			"<label for='startyear'>Year:</label>", popup_menu(	-name=>'startyear',
										-id=>'startyear',
										-value=>[@startyears],
										-default=>"$startyear"),
		),
		fieldset(
		legend('End Date'),
			"<label for='endday'>Day:</label>", popup_menu(	-name=>'endday',
										-id=>'endday',
										-value=>[@days],
										-default=>"$endday"),
			"<label for='endmonth'>Month:</label>", popup_menu(	-name=>'endmonth',
										-id=>'endmonth',
										-value=>[@months],
										-default=>"$endmonth"),
			"<label for='endyear'>Year:</label>", popup_menu(	-name=>'endyear',
										-id=>'endyear',
										-value=>[@endyears],
										-default=>"$endyear"),
		),
		fieldset(
		legend('Location Details'),
		"<label for='streetaddress'>Street Address:</label>", textfield(	-name=>'streetaddress',
								-id=>'streetaddress',
								-maxlength=>60,
								-default=>"$streetaddress"),
		"<label for='postcode'>Post Code:</label>", textfield(	-name=>'postcode',
								-id=>'postcode',
								-maxlength=>4,
								-default=>"$postcode"),
		"<label for='suburb'>Suburb:</label>", textfield(	-name=>'suburb',
								-id=>'suburb',
								-maxlength=>20,
								-default=>"$suburb"),
		"<label for='state'>State:</label>", popup_menu(	-name=>'state',
									-id=>'state',
									-value=>[@states],
									-default=>"$state"),
		),
		fieldset(
		submit('submit', "$submitText"),
		reset('reset', 'Reset'),
		),
		end_form,
	);
}


sub parseJobForm {
	@job = @_;
	($jobtitle, $frompaybracket, $topaybracket, $status, $description, $startday, $startmonth, $startyear, $endday, $endmonth, $endyear, $streetaddress, $postcode, $suburb, $state) = @job;
	push @errors, checkMissingValueForm($jobtitle,"Job Title");
	push @errors, checkMissingValueForm($frompaybracket,"Lowest Salary");
	push @errors, checkNumericForm($frompaybracket,"Lowest Salary");
	push @errors, checkNumericForm($topaybracket,"Highest Salary");
	push @errors, checkMissingValueForm($topaybracket,"Highest Salary");
	if ($frompaybracket > $topaybracket) {
		$error = "<em>Lowest Salary</em> can not exceed <em>Highest Salary</em>.";
		push @errors, $error;
	}
	push @errors, checkMissingValueForm($description,"Description");
	push @errors, checkMissingValueForm($status,"Job Status");
	push @errors, checkMissingValueForm($startday,"Start Date");
	push @errors, checkNumericForm($startday, "Start Date");
	push @errors, checkMissingValueForm($startmonth,"Start Date");
	push @errors, checkNumericForm($startmonth,"Start Date");
	push @errors, checkMissingValueForm($startyear,"Start Date");
	push @errors, checkNumericForm($startyear,"Start Date");
	push @errors, checkMissingValueForm($endday,"End Date");
	push @errors, checkNumericForm($endday,"End Date");
	push @errors, checkMissingValueForm($endmonth,"End Date");
	push @errors, checkNumericForm($endmonth,"End Date");
	push @errors, checkMissingValueForm($endyear,"End Date");
	push @errors, checkNumericForm($endyear,"End Date");
	push @errors, checkMissingValueForm($streetaddress,"Street Address");
	push @errors, checkMissingValueForm($postcode,"Postcode");
	push @errors, checkValidPostcodeForm($postcode,"Post Code");
	push @errors, checkMissingValueForm($suburb,"Suburb");
	push @errors, checkMissingValueForm($state,"State");
	#push @errors, checkAlphaNumericForm($company, "Company");
	#push @errors, checkMissingValueForm($contactname,"Contact Name");
	#push @errors, checkAlphaNumericForm($contactname,"Contact Name");
	#push @errors, checkMissingValueForm($companyposition,"Company Position");
	#push @errors, checkAlphaNumericForm($companyposition,"Company Position");
	#push @errors, checkMissingValueForm($email,"Email");
	#push @errors, checkValidEmailForm($email,"Email");
	#push @errors, checkMissingValueForm($phonenumber,"Phone Number");
	#push @errors, checkNumericForm($phonenumber,"Phone Number");
	#push @errors, checkMissingValueForm($password,"Password");
	#push @errors, checkAlphaNumericForm($password,"Password");
	#push @errors, checkMissingValueForm($confirmpassword,"Confirm Password");
	#push @errors, checkAlphaNumericForm($confirmpassword,"Confirm Password");
	return @errors;
}

sub jobFormErrors
{
	@errors = parseJobForm(param('jobtitle'), param('frompaybracket'), param('topaybracket'), param('status'), param('description'), param('startday'), param('startmonth'), param('startyear'), param('endday'), param('endmonth'), param('endyear'), param('streetaddress'), param('postcode'), param('suburb'), param('state'));

	if ($#errors > -1) {
		print "<ul>\n";
		$validated = 1;
		foreach $error (@errors) {
			 if (length($error)) {
				$validated = 0;
                         	print li($error);
			}
                }
		print "</ul>\n";
	}
	if (!$validated) {
		jobForm();
	}
	else
	{
		if ($submitText eq "Post Job") { 
			addJob(); 
		}
		elsif ($submitText eq "Update Job") { 
			updateJob($id); 
		}
	}
}

sub addJob {
	#change me
	$employerid = 1;
	$dbh = DBI->connect("dbi:Pg:dbname='$dbName';host='$dbHost';port='$dbPort';port='$dbPort'", "$dbUser", "$dbPass", { RaiseError => 1, AutoCommit => 1 }) || die "Error connecting to the database: $DBI::errstr\n";
	$query = "INSERT into job (jobid, jobtitle, description, status, frompaybracket, topaybracket, startdate, enddate, employerid, streetaddress, postcode, suburb, state) values (nextval('job_jobid_seq'::regclass), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        $sth = $dbh->prepare($query);
	$sth->bind_param(1, "$jobtitle");
	$sth->bind_param(2, "$description");
	$sth->bind_param(3, "$status");
	$sth->bind_param(4, "$frompaybracket");
	$sth->bind_param(5, "$topaybracket");
	$sth->bind_param(6, "$startyear-$startmonth-$startday");
	$sth->bind_param(7, "$endyear-$endmonth-$endday");
	$sth->bind_param(8, "$employerid");
	$sth->bind_param(9, "$streetaddress");
	$sth->bind_param(10, "$postcode");
	$sth->bind_param(11, "$suburb");
	$sth->bind_param(12, "$state");
        $sth->execute;
	print "Position <strong>$jobtitle</strong> successfully posted.";
}

sub updateJob {
	($id) = @_;
	$dbh = DBI->connect("dbi:Pg:dbname='$dbName';host='$dbHost';port='$dbPort';port='$dbPort'", "$dbUser", "$dbPass", { RaiseError => 1, AutoCommit => 1 }) || die "Error connecting to the database: $DBI::errstr\n";
	$query = "UPDATE job set jobtitle=?, description=?, status=?, frompaybracket=?, topaybracket=?, startdate=?, enddate=?, streetaddress=?, postcode=?, suburb=?, state=? where jobid=?";
        $sth = $dbh->prepare($query);
	$sth->bind_param(1, "$jobtitle");
	$sth->bind_param(2, "$description");
	$sth->bind_param(3, "$status");
	$sth->bind_param(4, "$frompaybracket");
	$sth->bind_param(5, "$topaybracket");
	$sth->bind_param(6, "$startyear-$startmonth-$startday");
	$sth->bind_param(7, "$endyear-$endmonth-$endday");
	$sth->bind_param(8, "$streetaddress");
	$sth->bind_param(9, "$postcode");
	$sth->bind_param(10, "$suburb");
	$sth->bind_param(11, "$state");
	$sth->bind_param(12, "$id");
        $sth->execute;
	print "<strong>$jobtitle</strong> has successfully been updated.";
}

sub getJobDetails {
	($jobid) = @_;
	#input validation on student, should never be false.
	if(!( $jobid =~ m/^\d{1,}$/)) {
		return NULL;
	}
	$dbh = DBI->connect("dbi:Pg:dbname='$dbName';host='$dbHost';port='$dbPort';port='$dbPort'", "$dbUser", "$dbPass", { RaiseError => 1, AutoCommit => 1 }) || die "Error connecting to the database: $DBI::errstr\n";
        $query = "SELECT * FROM job where jobid = ?";
        $sth = $dbh->prepare($query);
	$sth->bind_param(1, "$jobid");
        $sth->execute;
	@job = $sth->fetchrow;
	return @job;
}
##### End job form code
#
#### Begin grade form code
sub gradeForm {
	@years = (1998 .. 2006);
	@semesters = ('Semester 1', 'Semester 2');
	@months = (1 .. 12);
	print
	div( {-class=> "form"},
 		start_form,
		fieldset(
		legend('Transcript Details'),
		"<label for='subject'>Subject Name:</label>", textfield(	-name=>'subject',
								-id=>'subject',
								-maxlength=>60,
								-default=>"$subject"),
		"<label for='grade'>Grade:</label>", textfield(	-name=>'grade',
								-id=>'grade',
								-maxlength=>3,
								-default=>"$grade"),
		),
		fieldset(
		legend('Completion Time'),
			"<label for='year'>Year:</label>", popup_menu(	-name=>'year',
										-id=>'year',
										-value=>[@years],
										-default=>"$year"),
			"<label for='semester'>Semester:</label>", popup_menu(	-name=>'semester',
										-id=>'semester',
										-value=>[@semesters],
										-default=>"$semester"),
		),
		fieldset(
		submit('submit', "$submitText"),
		reset('reset', 'Reset'),
		),
		end_form,
	);
}


sub parseGradeForm {
	@grade = @_;
	($subject, $grade, $year, $semester) = @grade;
	push @errors, checkMissingValueForm($subject,"Subject Name");
	push @errors, checkAlphaNumericForm($subject,"Subject Name");
	push @errors, checkMissingValueForm($grade,"Grade");
	push @errors, checkNumericForm($grade,"Grade");
	if ($grade > 100 || $grade < 0) {
		$error = "Invalid <em>Grade</em> entered. Must be between 0 and 100.";
		push @errors, $error;
	}
	push @errors, checkMissingValueForm($year,"Year");
	push @errors, checkNumericForm($year,"Year");
	push @errors, checkMissingValueForm($semester,"Semester");
	#Semester in 1,2
	return @errors;
}

sub gradeFormErrors
{
	@errors = parseGradeForm(param('subject'), param('grade'), param('year'), param('semester'));

	if ($#errors > -1) {
		print "<ul>\n";
		$validated = 1;
		foreach $error (@errors) {
			 if (length($error)) {
				$validated = 0;
                         	print li($error);
			}
                }
		print "</ul>\n";
	}
	if (!$validated) {
		gradeForm();
	}
	else
	{
		if ($submitText eq "Add Subject") { 
			addGrade(); 
		}
		elsif ($submitText eq "Update Subject") { 
			updateGrade($id); 
		}
	}
}

sub addGrade {
	#change me
	$studentid = 1;
	if ($semester eq "Semester 1") { $semester = 1;}
	elsif ($semester eq "Semester 2") { $semester = 2;}
	$dbh = DBI->connect("dbi:Pg:dbname='$dbName';host='$dbHost';port='$dbPort';port='$dbPort'", "$dbUser", "$dbPass", { RaiseError => 1, AutoCommit => 1 }) || die "Error connecting to the database: $DBI::errstr\n";
	$query = "INSERT into subjectGrade values (nextval('subjectGrade_gradeid_seq'::regclass), ?, ?, ?, ?, ?)";
        $sth = $dbh->prepare($query);
	$sth->bind_param(1, "$studentid");
	$sth->bind_param(2, "$subject");
	$sth->bind_param(3, "$grade");
	$sth->bind_param(4, "$year");
	$sth->bind_param(5, "$semester");
        $sth->execute;
	print "Subject <strong>$subject</strong> successfully added.";
}

sub updateGrade {
	($id) = @_;
	if ($semester eq "Semester 1") { $semester = 1;}
	elsif ($semester eq "Semester 2") { $semester = 2;}
	$dbh = DBI->connect("dbi:Pg:dbname='$dbName';host='$dbHost';port='$dbPort';port='$dbPort'", "$dbUser", "$dbPass", { RaiseError => 1, AutoCommit => 1 }) || die "Error connecting to the database: $DBI::errstr\n";
	$query = "UPDATE subjectGrade set subject=?, grade=?, year=?, semester=? where gradeid=?";
        $sth = $dbh->prepare($query);
	$sth->bind_param(1, "$subject");
	$sth->bind_param(2, "$grade");
	$sth->bind_param(3, "$year");
	$sth->bind_param(4, "$semester");
	$sth->bind_param(5, "$id");
	
        $sth->execute;
	print "Subject <strong>$subject</strong> successfully updated.";
}

sub getGradeDetails {
	($gradeid) = @_;
	#input validation on gradeid, should never be false.
	if(!( $gradeid =~ m/^\d{1,}$/)) {
		return NULL;
	}
	$dbh = DBI->connect("dbi:Pg:dbname='$dbName';host='$dbHost';port='$dbPort';port='$dbPort'", "$dbUser", "$dbPass", { RaiseError => 1, AutoCommit => 1 }) || die "Error connecting to the database: $DBI::errstr\n";
        $query = "SELECT * FROM subjectGrade where gradeid = ?";
        $sth = $dbh->prepare($query);
	$sth->bind_param(1, "$gradeid");
        $sth->execute;
	@grade = $sth->fetchrow;
	return @grade;
}
##### End grade form code
#
sub resetPasswordForm
{
	print p("Enter your <strong>email</strong> to have secret code sent to you. When you get your code, come back here and put it in the <strong>secret</strong> field to have your new password emailed to you.");

	print
	div( {-class=> "form"},
 		start_form,
		fieldset(
		legend('Email'),
		"<label for='email'>Email:</label>", textfield(	-name=>'email',
								-id=>'email',
								-maxlength=>60,
								-default=>"$email"),br,
		"<label for='type'>Type:</label>", radio_group( -name=>'type',
								-id=>'type',
								-values=>['Student', 'Employer'],
                                -default=>'Student'),
		),
		fieldset(
		legend('Secret'),
		"<label for='secret'>Secret:</label>", textfield(	-name=>'secret',
								-id=>'secret',
								-maxlength=>60,
								-default=>"$secret"),
		),
		fieldset(
		submit('submit', "Reset Password"),
		reset('reset', 'Reset'),
		),
		end_form
	);
}

sub parseResetPasswordForm {
	($email, $type, $secret) = @_;
	push @errors, checkMissingValueForm($email,"Email");
	push @errors, checkValidEmailForm($email,"Email");
	push @errors, checkMissingValueForm($type,"Type");
	return @errors;
}

sub resetPasswordFormErrors {
	@errors = parseResetPasswordForm(param('email'), param('type'), param('secret'));

	if ($#errors > -1) {
		print "<ul>\n";
		$validated = 1;
		foreach $error (@errors) {
			 if (length($error)) {
				$validated = 0;
                         	print li($error);
			}
                }
		print "</ul>\n";
	}
	if (!$validated) {
		resetPasswordForm();
	}
	else
	{
		if (length($secret) > 1) {
			resetPasswordEmail($email, $type, $secret);
		}
		else {
			resetPassword($email, $type);
		}
	}
}

sub resetPasswordEmail {
	($email, $type, $secret) = @_;
	$dbh = DBI->connect("dbi:Pg:dbname='$dbName';host='$dbHost';port='$dbPort';port='$dbPort'", "$dbUser", "$dbPass", { RaiseError => 1, AutoCommit => 1 }) || die "Error connecting to the database: $DBI::errstr\n";
	if ($type eq "Student") {
		$usertype = "S";
		$query2 = "UPDATE student set password = ? where studentid = ?";
	}
	elsif ($type eq "Employer") {
		$usertype = "E";
		$query2 = "UPDATE employer set password = ? where employerid = ?";
	}
	else {
		$usertype = "X";
	}
       	$query = "SELECT id FROM resetpassword where resetcode = ? and usertype = ? limit 1";
        $sth = $dbh->prepare($query);
	$sth->bind_param(1, "$secret");
	$sth->bind_param(2, "$usertype");
        $sth->execute;
	if ($user = $sth->fetchrow) {
		$newpass = md5_hex(rand(99999999999999999999999999999999));
        	$sth = $dbh->prepare($query2);
		$sth->bind_param(1, "$newpass");
		$sth->bind_param(2, "$user");
        	$sth->execute;
		my $subject  = "Subject: Unijobs.com.au new password.\n";
		my $content  = "Here's your password for $email. Password: $newpass User: $user Type: $type\n";
		open(SENDMAIL, "|$sendmail") or die "Cannot open $sendmail: $!";
		print SENDMAIL $to;
		#print SENDMAIL "To: ivan.kk\@gmail.com\n";
		print SENDMAIL $from;
		print SENDMAIL $subject;
		print SENDMAIL "Content-type: text/plain\n\n";
		print SENDMAIL $content;
		close(SENDMAIL);
		print "New password has been sent to <strong>$email</strong>.";
		$query = "DELETE from resetpassword where id = ? and usertype=?";
        	$sth = $dbh->prepare($query);
		$sth->bind_param(1, "$user");
		$sth->bind_param(2, "$usertype");
        	$sth->execute;
	}
	else {
		print "Sorry incorrect email or reset code.";
	}
}

sub resetPassword {
	($email, $type) = @_;
	$dbh = DBI->connect("dbi:Pg:dbname='$dbName';host='$dbHost';port='$dbPort';port='$dbPort'", "$dbUser", "$dbPass", { RaiseError => 1, AutoCommit => 1 }) || die "Error connecting to the database: $DBI::errstr\n";
	if ($type eq "Student") {
        	$query = "SELECT studentid FROM student where email = ? limit 1";
		$usertype = "S";
	}
	elsif ($type eq "Employer") {
        	$query = "SELECT employerid FROM employer where email = ? limit 1";
		$usertype = "E";
	}
	else {
		$query = "SELECT 1 where 1 = ?";
	}
        $sth = $dbh->prepare($query);
	$sth->bind_param(1, "$email");
        $sth->execute;
	if ($user = $sth->fetchrow) {
		$secret = md5_hex(rand(99999999999999999999999999999999));
		$ip = $ENV{REMOTE_ADDR};
		my $subject  = "Subject: Unijobs.com.au password reset.\n";
		my $content  = "Here's your password reset code for $email requested by $ip. Password http://unijobs.com.au/reset?$secret&email=$email. Or Enter $secret at the reset form.\n";
		open(SENDMAIL, "|$sendmail") or die "Cannot open $sendmail: $!";
		print SENDMAIL $to;
		#print SENDMAIL "To: ivan.kk\@gmail.com\n";
		print SENDMAIL $from;
		print SENDMAIL $subject;
		print SENDMAIL "Content-type: text/plain\n\n";
		print SENDMAIL $content;
		close(SENDMAIL);
		print "Reset instructions have been sent to <strong>$email</strong>. Your IP has been logged to prevent abuse.";
		$query = "INSERT into resetpassword values (?, ?, ?)";
        	$sth = $dbh->prepare($query);
		$sth->bind_param(1, "$user");
		$sth->bind_param(2, "$secret");
		$sth->bind_param(3, "$usertype");
        	$sth->execute;
	}
	else {
		print "Sorry <strong>$email</strong> wasn't found in our <strong>$type</strong> database.";
	}
}

##### Begin search student form code

sub searchStudentForm {

	@months = (1 .. 12);
	#Todo:
	#@months = qw( Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec );
	@days = (1 .. 31);
	@yearscommenced = (2000 .. 2006);
	@afyears = (2006 .. 2010);
	@atyears = (2006 .. 2016);
	@completionyears = (2006 .. 2016);
	($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
	if (!$afday && !$atday) { $afday = $atday = $mday;}
	if (!$afmonth && !$atmonth) { $afmonth = $atmonth = $mon+1;}
	if (!$afyear && !$atyear) { $afyear = $atyear = $year+1900;}

	print
		div( {-class=> "form"},
 			start_form,
			fieldset(
			legend('Degree Details'),
			"<label for='gpa'>GPA:</label>", textfield(	-name=>'gpa',
								-id=>'gpa',
								-maxlength=>50,
								-default=>"$gpa"),
		"<label for='degree'>Degree:</label>", textfield(	-name=>'degree',
								-id=>'degree',
								-maxlength=>60,
								-default=>"$degree"),
		"<label for='yearcommenced'>Year Commenced:</label>", popup_menu(	-name=>'yearcommenced',
								-id=>'yearcommenced',
								-value=>[@yearscommenced],
								-default=>"$yearcommenced"),
			),
			fieldset(
			legend('Available From'),
			"<label for='afday'>Day:</label>", popup_menu(	-name=>'afday',
										-id=>'afday',
										-value=>[@days],
										-default=>"$afday"),
			"<label for='afmonth'>Month:</label>", popup_menu(	-name=>'afmonth',
										-id=>'afmonth',
										-value=>[@months],
										-default=>"$afmonth"),
			"<label for='afyear'>Year:</label>", popup_menu(	-name=>'afyear',
										-id=>'afyear',
										-value=>[@afyears],
										-default=>"$afyear"),
			),
			fieldset(
			legend('Available Till'),
			"<label for='atday'>Day:</label>", popup_menu(	-name=>'atday',
										-id=>'atday',
										-value=>[@days],
										-default=>"$atday"),
			"<label for='atmonth'>Month:</label>", popup_menu(	-name=>'atmonth',
										-id=>'atmonth',
										-value=>[@months],
										-default=>"$atmonth"),
			"<label for='atyear'>Year:</label>", popup_menu(	-name=>'atyear',
										-id=>'atyear',
										-value=>[@atyears],
										-default=>"$atyear"),
			),
			fieldset( 
			legend('Keywords'),
			"<label for='keywords'>Keywords:</label>", textarea(	-rows=> '10',
							-cols=>'20',
							-name=>'keywords',
							-id=>'keywords',
							-default=>"$keywords"),
			),
			fieldset(
			submit('submit', "$submitText"),
			reset('reset', 'Reset'),
			),
			end_form,
		);
}

sub parseSearchStudentForm {
	@student = @_;
	($gpa, $degree, $yearcommenced, $afday, $afmonth, $afyear, $atday, $atmonth, $atyear, $keywords) = @student;
	#push @errors, checkNumericForm($gpa,"GPA");
	#push @errors, checkMissingValueForm($gpa,"GPA");
	#push @errors, checkAlphaNumericForm($degree,"Degree");
	#push @errors, checkMissingValueForm($degree,"Degree");
	push @errors, checkMissingValueForm($yearcommenced,"Year Commenced");
	push @errors, checkNumericForm($yearcommenced,"Year Commenced");
	if ($gpa > 100 || $gpa < 0) {
		$error = "Invalid <em>GPA</em> entered. Must be between 0 and 100.";
		push @errors, $error;
	}
	#more form validation goes here.
	return @errors;
}

sub searchStudentFormErrors
{
	@errors = parseSearchStudentForm(param('gpa'), param('degree'),param('yearcommenced'), param('afday'), param('afmonth'), param('afyear'), param('atday'), param('atmonth'), param('atyear'),param('keywords'));
	if ($#errors > -1) {
		print "<ul>\n";
		$validated = 1;
		foreach $error (@errors) {
			 if (length($error)) {
				$validated = 0;
                         	print li($error);
			}
                }
		print "</ul>\n";
	}
	if (!$validated) {
		searchStudentForm();
	}
	else
	{
		@students = searchForStudents(param('gpa'), param('degree'),param('yearcommenced'), param('afday'), param('afmonth'), param('afyear'), param('atday'), param('atmonth'), param('atyear'),param('keywords'));
	}
}

sub searchForStudents {
	@searchTerms = @_;
	($gpa, $degree, $yearcommenced, $afday, $afmonth, $afyear, $atday, $atmonth, $atyear, $keywords) = @searchTerms;

	$dbh = DBI->connect("dbi:Pg:dbname='$dbName';host='$dbHost'", "$dbName", "$dbPass", { RaiseError => 1, AutoCommit => 1 }) || die "Error connecting to the database: $DBI::errstr\n";
		
	if (length($gpa) < 1) {
		$gpa = '0';
	}
	if (length($degree) < 1) {
		$degree = '';
	}
	$query = "SELECT studentid, firstname, lastname, email, phonenumber FROM Student WHERE gpa >= ? AND degree LIKE ? AND yearcommenced = ? AND keywords LIKE ? AND availablefrom <= ? AND availabletill >= ?"; 
  	$sth = $dbh->prepare($query);
	$sth->bind_param(1, "$gpa");
	$sth->bind_param(2, "%$degree%");
	$sth->bind_param(3, "$yearcommenced");
	$sth->bind_param(4, "%$keywords%");
	$sth->bind_param(5, "$afyear-$afmonth-$afday");
	$sth->bind_param(6, "$atyear-$atmonth-$atday");
   	$sth->execute;
	if(@student = $sth->fetchrow) {
		print 	'<table >',
			Tr({-align=>center,-valign=>top},
			[
				th(['First Name','Last Name','Email', 'Phone Number'])
			]
 		);
		print "<tr align='center' valign='top' >".td(["<a href='showstudent.cgi?id=$student[0]'>$student[1]</a>", "<a href='showstudent.cgi?id=$student[0]'>$student[2]</a>", "$student[3]", "$student[4]"])."</tr>";
		while (@student = $sth->fetchrow)
		{
			print "<tr align='center' valign='top' >".td(["<a href='showstudent.cgi?id=$student[0]'>$student[1]</a>", "<a href='showstudent.cgi?id=$student[0]'>$student[2]</a>", "$student[3]", "$student[4]"])."</tr>";
		
		}
		print '</table>';
	}
	else {
		print "There are no students matching your search criteria.";
	}
	$dbh->disconnect();
	return @student;

}

##### End search student form code

##### Begin search job form code

sub searchJobForm {
	@months = (1 .. 12);
	@states = qw(NSW VIC QLD SA WA NT ACT TAS);
	@days = (1 .. 31);
	@yearscommenced = (2000 .. 2006);
	@statuses = qw(Hiring Interviewing Filled);
	@startyears = (2006 .. 2010);
	@endyears = (2006 .. 2016);
	($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
	if (!$startday && !$endday) { $startday = $endday = $mday;}
	if (!$startmonth && !$endmonth) { $startmonth = $endmonth = $mon+1;}
	if (!$startyear && !$endyear) { $startyear = $endyear = $year+1900;}
	print
	div( {-class=> "form"},
 		start_form,
		fieldset(
		legend('Job Details'),
		"<label for='jobtitle'>Job Title:</label>", textfield(	-name=>'jobtitle',
								-id=>'jobtitle',
								-maxlength=>60,
								-default=>"$jobtitle"),
		"<label for='salary'>Salary:</label>", textfield(	-name=>'salary',
								-id=>'salary',
								-maxlength=>30,
								-default=>"$salary"),
		),
		fieldset(
		legend('Keywords'),
		"<label for='keywords'>Keywords:</label>", textarea(	-rows=> '10',
						-cols=>'20',
						-name=>'keywords',
						-id=>'keywords',
						-default=>"$keywords"),
		),
		fieldset(
		legend('Start Date'),
			"<label for='startday'>Day:</label>", popup_menu(	-name=>'startday',
										-id=>'startday',
										-value=>[@days],
										-default=>"$startday"),
			"<label for='startmonth'>Month:</label>", popup_menu(	-name=>'startmonth',
										-id=>'startmonth',
										-value=>[@months],
										-default=>"$startmonth"),
			"<label for='startyear'>Year:</label>", popup_menu(	-name=>'startyear',
										-id=>'startyear',
										-value=>[@startyears],
										-default=>"$startyear"),
		),
		fieldset(
		legend('End Date'),
			"<label for='endday'>Day:</label>", popup_menu(	-name=>'endday',
										-id=>'endday',
										-value=>[@days],
										-default=>"$endday"),
			"<label for='endmonth'>Month:</label>", popup_menu(	-name=>'endmonth',
										-id=>'endmonth',
										-value=>[@months],
										-default=>"$endmonth"),
			"<label for='endyear'>Year:</label>", popup_menu(	-name=>'endyear',
										-id=>'endyear',
										-value=>[@endyears],
										-default=>"$endyear"),
		),
		fieldset(
		legend('Location Details'),
		"<label for='state'>State:</label>", popup_menu(	-name=>'state',
									-id=>'state',
									-value=>[@states],
									-default=>"$state"),
		),
		fieldset(
		submit('submit', "$submitText"),
		reset('reset', 'Reset'),
		),
		end_form,
	);
}

sub searchJobFormErrors
{
	@errors = parseSearchJobForm(param('jobtitle'), param('salary'), param('keywords'), param('startday'), param('startmonth'), param('startyear'), param('endday'), param('endmonth'), param('endyear'), param('state'));

	if ($#errors > -1) {
		print "<ul>\n";
		$validated = 1;
		foreach $error (@errors) {
			 if (length($error)) {
				$validated = 0;
                         	print li($error);
			}
                }
		print "</ul>\n";
	}
	if (!$validated) {
		searchJobForm();
	}
	else
	{
		@job = searchForJobs(param('jobtitle'), param('salary'), param('keywords'), param('startday'), param('startmonth'), param('startyear'), param('endday'), param('endmonth'), param('endyear'), param('state'));
	}
}

sub parseSearchJobForm {
	@job = @_;
	($jobtitle, $salary, $keywords, $startday, $startmonth, $startyear, $endday, $endmonth, $endyear, $state) = @job;
	push @errors, checkMissingValueForm($startday,"Start Date");
	push @errors, checkNumericForm($startday, "Start Date");
	push @errors, checkMissingValueForm($startmonth,"Start Date");
	push @errors, checkNumericForm($startmonth,"Start Date");
	push @errors, checkMissingValueForm($startyear,"Start Date");
	push @errors, checkNumericForm($startyear,"Start Date");
	push @errors, checkMissingValueForm($endday,"End Date");
	push @errors, checkNumericForm($endday,"End Date");
	push @errors, checkMissingValueForm($endmonth,"End Date");
	push @errors, checkNumericForm($endmonth,"End Date");
	push @errors, checkMissingValueForm($endyear,"End Date");
	push @errors, checkNumericForm($endyear,"End Date");
	push @errors, checkMissingValueForm($state,"State");
	return @errors;

sub searchForJobs {
	@job = @_;
	($jobtitle, $salary, $keywords, $startday, $startmonth, $startyear, $endday, $endmonth, $endyear, $state) = @job;
	$dbh = DBI->connect("dbi:Pg:dbname='$dbName';host='$dbHost'", "$dbName", "$dbPass", { RaiseError => 1, AutoCommit => 1 }) || die "Error connecting to the database: $DBI::errstr\n";
	if (length($jobtitle) < 1) {
		$jobtitle = '';
	}
	if (length($salary) < 1) {
		$salary = '0';
	}
	$query = "SELECT jobid, jobtitle, frompaybracket, topaybracket, suburb FROM Job WHERE jobtitle LIKE ? AND topaybracket > ? AND description LIKE ? AND status = 'Hiring' AND state = ? AND startdate >= ? AND enddate <= ?"; 
  	$sth = $dbh->prepare($query);
	$sth->bind_param(1, "%$jobtitle%");
	$sth->bind_param(2, "$salary");
	$sth->bind_param(3, "%$keywords%");
	$sth->bind_param(4, "$state");
	$sth->bind_param(5, "$startyear-$startmonth-$startday");
	$sth->bind_param(6, "$endyear-$endmonth-$endday");
   	$sth->execute;
	if(@job = $sth->fetchrow) {
		print 	'<table >',
			Tr({-align=>center,-valign=>top},
			[
				th(['Job Title','Lowest Salary','Highest Salary', 'Suburb'])
			]
 		);
		print "<tr align='center' valign='top' >".td(["<a href='showjob.cgi?id=$job[0]'>$job[1]</a>", "$job[2]", "$job[3]", "$job[4]"])."</tr>";
		while (@job = $sth->fetchrow)
		{
			print "<tr align='center' valign='top' >".td(["<a href='showjob.cgi?id=$job[0]'>$job[1]</a>", "$job[2]", "$job[3]", "$job[4]"])."</tr>";
		
		}
		print '</table>';
	}
	else {
		print "There are no jobs matching your search criteria.";
	}
	$dbh->disconnect();
	return @job;

}
}

##### End search job form code

##### Start show students code

sub showStudents {
	$id = url_param('id');
	$dbh = DBI->connect("dbi:Pg:dbname='$dbName';host='$dbHost'", "$dbName", "$dbPass", { RaiseError => 1, AutoCommit => 1 }) || die "Error connecting to the database: $DBI::errstr\n";
	$query = "SELECT * FROM Student WHERE studentid = ?";
	$sth = $dbh->prepare($query);
	$sth->bind_param(1, "$id");
	$sth->execute;

	print 	'<table >',
		Tr({-align=>center,-valign=>top},
		[
			th(['Student Details'])
		]
 	);
	@student = $sth->fetchrow;
	{
		print "<tr align='center' valign='top' >".td(["First Name:", "$student[1]"])."</tr>";
		print "<tr align='center' valign='top' >".td(["Last Name:", "$student[2]"])."</tr>";
		print "<tr align='center' valign='top' >".td(["Email:", "$student[3]"])."</tr>";
		print "<tr align='center' valign='top' >".td(["Date Of Birth:", "$student[4]"])."</tr>";
		print "<tr align='center' valign='top' >".td(["Phone Number:", "$student[5]"])."</tr>";
		print "<tr align='center' valign='top' >".td(["Street Address:", "$student[7]"])."</tr>";
		print "<tr align='center' valign='top' >".td(["Suburb:", "$student[10]"])."</tr>";
		print "<tr align='center' valign='top' >".td(["State:", "$student[9]"])."</tr>";
		print "<tr align='center' valign='top' >".td(["Post Code:", "$student[8]"])."</tr>";
		print "<tr align='center' valign='top' >".td(["GPA:", "$student[11]"])."</tr>";
		print "<tr align='center' valign='top' >".td(["Degree:", "$student[12]"])."</tr>";
		print "<tr align='center' valign='top' >".td(["Major:", "$student[13]"])."</tr>";
		print "<tr align='center' valign='top' >".td(["Year Commenced:", "$student[14]"])."</tr>";
		print "<tr align='center' valign='top' >".td(["Completion Year:", "$student[15]"])."</tr>";
		print "<tr align='center' valign='top' >".td(["Available From:", "$student[17]"])."</tr>";
		print "<tr align='center' valign='top' >".td(["Available Until:", "$student[18]"])."</tr>";
	}
	$dbh->disconnect();
	print '</table>';

}
##### End show students code

sub showJobs {
	$id = url_param('id');
	$dbh = DBI->connect("dbi:Pg:dbname='$dbName';host='$dbHost'", "$dbName", "$dbPass", { RaiseError => 1, AutoCommit => 1 }) || die "Error connecting to the database: $DBI::errstr\n";
	$query = "SELECT * FROM Job J, Employer E WHERE J.Jobid = ? AND J.Employerid = E.Employerid";
	$sth = $dbh->prepare($query);
	$sth->bind_param(1, "$id");
	$sth->execute;

	print 	'<table >',
		Tr({-align=>center,-valign=>top},
		[
			th(['Job Details'])
		]
 	);
	@job = $sth->fetchrow;
	{
		print "<tr align='center' valign='top' >".td(["Job Title:", "$job[1]"])."</tr>";
		print "<tr align='center' valign='top' >".td(["Description:", "$job[2]"])."</tr>";
		print "<tr align='center' valign='top' >".td(["Date Listed:", "$job[3]"])."</tr>";
		print "<tr align='center' valign='top' >".td(["Status:", "$job[4]"])."</tr>";
		print "<tr align='center' valign='top' >".td(["Lowest Salary:", "$job[5]"])."</tr>";
		print "<tr align='center' valign='top' >".td(["Highest Salary:", "$job[6]"])."</tr>";
		print "<tr align='center' valign='top' >".td(["Start Date:", "$job[7]"])."</tr>";
		print "<tr align='center' valign='top' >".td(["End Date:", "$job[8]"])."</tr>";
		print "<tr align='center' valign='top' >".td(["Street Address:", "$job[10]"])."</tr>";
		print "<tr align='center' valign='top' >".td(["Suburb:", "$job[12]"])."</tr>";
		print "<tr align='center' valign='top' >".td(["State:", "$job[13]"])."</tr>";
		print "<tr align='center' valign='top' >".td(["Post Code:", "$job[11]"])."</tr>";
		print "<tr align='center' valign='top' >".td(["Company:", "$job[16]"])."</tr>";
		print "<tr align='center' valign='top' >".td(["Contact Name:", "$job[17]"])."</tr>";
		print "<tr align='center' valign='top' >".td(["Phone Number:", "$job[18]"])."</tr>";
		print "<tr align='center' valign='top' >".td(["Email:", "$job[19]"])."</tr>";
		print "<tr align='center' valign='top' >".td(["Company Position:", "$job[20]"])."</tr>";

	}
	$dbh->disconnect();
	print '</table>';

}



##### Login form code
sub loginForm
{
	($session) = @_;
	getSession();
	print
	div( {-class=> "form"},
 		start_form,
		fieldset(
		legend('Login'),
		"<label for='userID'>User ID:</label>", textfield(	-name=>'userID',
								-id=>'userID',
								-maxlength=>60,
								-default=>""),
		"<label for='password'>Password:</label>", password_field(	-name=>'password',
								-id=>'password',
								-maxlength=>30,
								-default=>""),
		"<br><br><label for='user'>Login As:</label>", radio_group( -name=>'user',
								-values=>['Student', 'Employer'],
                                -default=>'Student'),
		),
		fieldset(
		submit('submit', "$submitText"),
		reset('reset', 'Reset'),
		),
		end_form
	);
	print p("Forgotten your password? Reset your",
		a({href=>"resetpassword.cgi"}, "password"), 
		" here."
		);
}

sub parseLoginForm {
	@login = @_;
	($userID, $password) = @login;
	push @errors, checkMissingValueForm($userID,"User ID");
	push @errors, checkNumericForm($userID, "User ID");
	push @errors, checkMissingValueForm($password,"Password");
	push @errors, checkAlphaNumericForm($password,"Password");
	return @errors;
}

sub loginFormErrors
{
	@errors = parseLoginForm(param('userID'), param('password'));
	if ($#errors > -1) {
		print "<ul>\n";
		$validated = 1;
		foreach $error (@errors) {
			 if (length($error)) {
				$validated = 0;
                print li($error);
			}
                }
		print "</ul>\n";
	}
	if (!$validated) {
		loginForm();
	}
	else
	{
		if ($submitText eq "Login") { 
			login(param('userID'), param('password'), param('user'));
		}
	}
}

sub login {
	($userID, $password, $user) = @_;
	#input validation on user, should never be false.
	if(!(( $userID =~ m/^\d{1,}$/) || ( $password =~ m/^\d{1,}$/)) ) {
		return NULL;
	}
	
	$dbh = DBI->connect("dbi:Pg:dbname='$dbName';host='$dbHost'", "$dbUser", "$dbPass", { RaiseError => 1, AutoCommit => 1 }) || die "Error connecting to the database: $DBI::errstr\n";
	if ($user eq 'Employer') 
	{
    	$query = "SELECT * FROM employer where employerid = ? and password = ?";
    }
    else
    {
    	$query = "SELECT * FROM student where studentid = ? and password = ?";
    }
    $sth = $dbh->prepare($query);
	$sth->bind_param(1, "$userID");
	$sth->bind_param(2, md5_hex($password));
	# MD5 stuff but passwords must be created in the right was
	# ie, insert into tableName (password) values (md5($password));
	# then replace
	# $sth->bind_param(2, "$password");
	# with
	# $sth->bind_param(2, "md5($password)");
	
    $sth->execute;
	@user = $sth->fetchrow;
	if (@user) 
	{
		if ($user eq 'Employer') 
		{
			# @user[3] = contactname
			print "Welcome back @user[3] <br>"; 
			setSession(@user[0], 'Employer');
			getSession();
		}
		elsif ($user eq 'Student') 
		{
			# @user[1] = firstname @user[2] = lastname
			print "Welcome back @user[1] @user[2] <br>"; 
			setSession(@user[0], 'Student');			
			getSession();
		}
	}
	else 
	{
		print "Not verified <br>";
		loginForm();
	}
}

sub getSession 
{
   if ($session) {
    my $userID = $session->param(-name=>'userID');
    my $userType = $session->param(-name=>'userType');
   }
    #$userID = 1;

    #print "<br>User ID: $userID<br>";
    #print "User Type: $userType";
    push @userSession, $userID;
    push @userSession, $userType;
    return @userSession;
}

# params are userID and userType = [Student | Employer]
sub setSession
{
	($userID, $userType) = @_;
	#$session = new CGI::Session();
	$session->param(-name=>'userID', -value=>$userID);
    $session->param(-name=>'userType', -value=>$userType);
}

sub logout
{
    $session->delete();
    print "User logged out<br>";
}
##### End Login Form code


##### Begin show transcript code

sub showTranscript {
	$id = url_param('id');
	$dbh = DBI->connect("dbi:Pg:dbname='$dbName';host='$dbHost'", "$dbName", "$dbPass", { RaiseError => 1, AutoCommit => 1 }) || die "Error connecting to the database: $DBI::errstr\n";
	$query = "SELECT * FROM Subjectgrade WHERE studentid = ?";
	$sth = $dbh->prepare($query);
	$sth->bind_param(1, "$id");
	$sth->execute;

	if(@transcript = $sth->fetchrow) {
		print 	'<table >',
			Tr({-align=>center,-valign=>top},
			[
				th(['Subject','Grade','Year', 'Semester'])
			]
 		);
		print "<tr align='center' valign='top' >".td(["$transcript[2]", "$transcript[3]", "$transcript[4]", "$transcript[5]"])."</tr>";
		while (@student = $sth->fetchrow)
		{
			print "<tr align='center' valign='top' >".td(["$transcript[2]", "$transcript[3]", "$transcript[4]", "$transcript[5]"])."</tr>";
		
		}
		print '</table>';
	}
	else {
		print "There are no listed subjects for this student.";
	}
	$dbh->disconnect();

}

##### End show transcript code


return 1;
