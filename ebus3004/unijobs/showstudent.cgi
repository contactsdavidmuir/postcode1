#!/usr/bin/perl
use CGI qw/:standard/;
use CGI::Pretty;
use unijobs;

print	header,
	start_html(	-title=>"Unijobs.com.au: Student Details",
			-style=>"unijobs.css");
print	"<div class='site'>",
	pheader(),
	h1("Student Details");
	showStudents();
	print "<br>";
	showTranscript();
print	pfooter(),
	"</div>",
	end_html;
