#!/usr/bin/perl
use CGI qw/:standard/;
use CGI::Pretty;
use unijobs;

print	header,
	start_html(	-title=>"Unijobs.com.au: Update Student Page",
			-style=>"unijobs.css");
print	"<div class='site'>",
	pheader(),
	h1("Update Student");
	if (!param) {
		userForm();
	}
	else {
		userFormErrors();
	}
print	pfooter(),
	"</div>",
	end_html;
