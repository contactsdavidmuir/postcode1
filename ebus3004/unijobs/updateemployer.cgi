#!/usr/bin/perl
use CGI qw/:standard/;
use CGI::Pretty;
use unijobs;

print	header,
	start_html(	-title=>"Unijobs.com.au: Update Employer",
			-style=>"unijobs.css");
print	"<div class='site'>",
	pheader(),
	h1("Update Employer");
	if (!param) {
		employerForm();
	}
	else {
		employerFormErrors();
	}
print	pfooter(),
	"</div>",
	end_html;
